'''
Modified version to make it work with PySide and Pyside2
'''

import xml.etree.ElementTree as xml
from cStringIO import StringIO


def findModule(module):
    ''' check to see if a module exists
    Args:
        module (str): the name of the module to check
    Returns:
        bool: whether the module is available for import or not
    '''
    import imp
    try:
        imp.find_module(module)
        return True
    except ImportError:
        return False


# this a quick fix for getting this runnin in maya with pyside2
# without breaking the other pyside 1 tools
if findModule('PySide'):
    import PySide
    import pysideuic
    class_cmd = 'PySide.QtGui.%s'
else:
    import PySide2 as PySide  # @UnresolvedImport
    import pyside2uic as pysideuic  # @UnresolvedImport
    # from PySide2 import QtUiTools
    import PySide2 as Qt
    class_cmd = 'PySide.QtWidgets.%s' 


def loadUiType(uiFile):
    """
    Pyside lacks the "loadUiType" command, so we have to convert the ui file to py code in-memory first
    and then execute it in a special frame to retrieve the form_class.
    """
    parsed = xml.parse(uiFile)
    widget_class = parsed.find('widget').get('class')
    form_class = parsed.find('class').text

    with open(uiFile, 'r') as f:
        o = StringIO()
        frame = {}

        pysideuic.compileUi(f, o, indent=0)
        pyc = compile(o.getvalue(), '<string>', 'exec')
        exec pyc in frame

        # Fetch the base_class and form class based on their type in the xml
        # from designer
        form_class = frame['Ui_%s' % form_class]
        base_class = eval(class_cmd % widget_class)
    return form_class, base_class


def loadUiType2(uiFile, widget):
    """Load an *.ui file. This function only works with PySide2. The advantage of this method is that it will add
    all the ui members as members of the widget
    
    Args:
        uiFile (str): ui file path
        widget (QWidget): Widget in which to embbed the ui file
    """

    loader = QtUiTools.QUiLoader()
    loadedUI = loader.load(uiFile, widget)


def loadUi(uifile, baseinstance=None):
    """Dynamically load a user interface from the given `uifile`
    This function calls `uic.loadUi` if using PyQt bindings,
    else it implements a comparable binding for PySide.
    Documentation:
        http://pyqt.sourceforge.net/Docs/PyQt5/designer.html#PyQt5.uic.loadUi
    Arguments:
        uifile (str): Absolute path to Qt Designer file.
        baseinstance (QWidget): Instantiated QWidget or subclass thereof
    Return:
        baseinstance if `baseinstance` is not `None`. Otherwise
        return the newly created instance of the user interface.
    """

    class _UiLoader(Qt.QtUiTools.QUiLoader):
        """Create the user interface in a base instance.
        Unlike `Qt._QtUiTools.QUiLoader` itself this class does not
        create a new instance of the top-level widget, but creates the user
        interface in an existing instance of the top-level class if needed.
        This mimics the behaviour of `PyQt5.uic.loadUi`.
        """

        def __init__(self, baseinstance):
            super(_UiLoader, self).__init__(baseinstance)
            self.baseinstance = baseinstance

        def load(self, uifile, *args, **kwargs):
            from xml.etree.ElementTree import ElementTree

            # For whatever reason, if this doesn't happen then
            # reading an invalid or non-existing .ui file throws
            # a RuntimeError.
            etree = ElementTree()
            etree.parse(uifile)

            widget = Qt.QtUiTools.QUiLoader.load(
                self, uifile, *args, **kwargs)

            # Workaround for PySide 1.0.9, see issue #208
            widget.parentWidget()

            return widget

        def createWidget(self, class_name, parent=None, name=""):
            """Called for each widget defined in ui file
            Overridden here to populate `baseinstance` instead.
            """

            if parent is None and self.baseinstance:
                # Supposed to create the top-level widget,
                # return the base instance instead
                return self.baseinstance

            # For some reason, Line is not in the list of available
            # widgets, but works fine, so we have to special case it here.
            # if class_name in self.availableWidgets() + ["Line"]:
                # Create a new widget for child widgets
            widget = Qt.QtUiTools.QUiLoader.createWidget(self,
                                                            class_name,
                                                            parent,
                                                            name)

            # else:
            #     raise Exception("Custom widget '%s' not supported"
            #                     % class_name)

            if self.baseinstance:
                # Set an attribute for the new child widget on the base
                # instance, just like PyQt5.uic.loadUi does.
                setattr(self.baseinstance, name, widget)

            return widget

    widget = _UiLoader(baseinstance).load(uifile)
    Qt.QtCore.QMetaObject.connectSlotsByName(widget)

    return widget

    # else:
    #     raise NotImplementedError("No implementation available for loadUi")    