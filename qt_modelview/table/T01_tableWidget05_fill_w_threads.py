'''
Simple Table Widget example
    It shows how to create a simple table view using QTableWidget,
    triggering the fill of the table with a button, and filling
    it from another thread using signals
'''
import sys
import time
from Qt import QtCore, QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E'],
            'rows': [1, 2, 3]
        }

        # Create Table Widget
        self.tableWidget = QtWidgets.QTableWidget(self)
        self.tableWidget.setRowCount(len(self.data['rows']))
        self.tableWidget.setColumnCount(len(self.data['cols']))
        self.tableWidget.setHorizontalHeaderLabels(self.data['cols'])

        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tableWidget)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        # Worker setup
        self.worker = Worker(self.data)
        self.worker.updateProgress.connect(self.progressBar.setValue)
        self.worker.addItem.connect(self.tableWidget.setItem)
        self.pushButton.clicked.connect(self.worker.start)

class Worker(QtCore.QThread):
    # This signals will be emitted during the processing.
    # The first one, updateProfress includes an int as argument
    # It lets the signal know to expect an integer argument when emitting.
    updateProgress = QtCore.Signal(int)
    # The second one, addItem includes two ints and a generic object as
    # the argument. The object can be any python object, like a dictionary
    addItem = QtCore.Signal(int, int, object)

    #You can do any extra things in this init you need, but for this example
    #nothing else needs to be done expect call the super's init
    def __init__(self, data):
        QtCore.QThread.__init__(self)
        self.data = data

    #A QThread is run by calling it's start() function, which calls this run()
    #function in it's own "thread". 
    def run(self):
        percent_inc = 100.0/(len(self.data['cols'])*len(self.data['rows']))
        percent = 0.0
        for row, row_name in enumerate(self.data['rows']):
            for col, col_name in enumerate(self.data['cols']):
                item_text = "{}{}".format(col_name, row_name)
                item = QtWidgets.QTableWidgetItem(item_text)
                self.addItem.emit(row, col, item)
                percent += percent_inc
                self.updateProgress.emit(int(percent))
                time.sleep(.25)
        self.updateProgress.emit(100)



def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()