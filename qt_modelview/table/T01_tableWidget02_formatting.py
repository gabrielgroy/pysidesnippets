'''
Simple Table Widget example
    It shows how to create a simple list view using QTableWidget
'''
import os
import sys
from Qt import QtWidgets, QtGui, QtCore

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E'],
            'rows': [1, 2, 3]
        }

        # Create Table Widget
        self.tableWidget = QtWidgets.QTableWidget(self)
        self.tableWidget.setRowCount(len(self.data['rows']))
        self.tableWidget.setColumnCount(len(self.data['cols']))
        self.tableWidget.setHorizontalHeaderLabels(self.data['cols'])
        self.tableWidget.setIconSize(QtCore.QSize(50,50))

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tableWidget)
        self.setLayout(layout)

        # Fill process
        for row, row_name in enumerate(self.data['rows']):
            for col, col_name in enumerate(self.data['cols']):
                item_text = "{}{}".format(col_name, row_name)
                item = QtWidgets.QTableWidgetItem(item_text)

                # Format
                if row == 0:
                    item.setIcon(QtGui.QIcon(icon_boat))
                elif row == 1:
                    item.setForeground(QtGui.QColor('red')) # or QtGui.QColor(QtCore.Qt.red)
                elif row == 2:
                    item.setTextAlignment(QtCore.Qt.AlignRight)
                    item.setFont(QtGui.QFont("Times", 20, QtGui.QFont.Bold))
                if col == 0:
                    item.setBackground(QtGui.QColor('yellow'))

                self.tableWidget.setItem(row, col, item)

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()