'''
Simple Table View example
    It shows how to create a simple list view using QTableView and QAbstractTableModel,
    triggering the fill of the table with a button, and filling
    it from another thread using signals
'''
import sys
import time
from Qt import QtCore, QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E'],
            'rows': [1, 2, 3]
        }

        # Create table, using the Model/View framework
        self.table_model = MyTableModel(self.data, self)
        self.table_view = QtWidgets.QTableView()
        
        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.table_view)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        # Connect the view with the model
        self.table_view.setModel(self.table_model)

        # Worker setup
        self.worker = Worker(self.data)
        self.worker.updateProgress.connect(self.progressBar.setValue)
        self.worker.addItem.connect(self.table_model.addItem)
        self.pushButton.clicked.connect(self.worker.start)

class MyTableModel(QtCore.QAbstractTableModel):
    def __init__(self, data, parent=None):
        """ This model will combine row and column in each cell
        """
        QtCore.QAbstractTableModel.__init__(self, parent)
        self.row_headers = data['rows']
        self.column_headers = data['cols']

        self._data = {}
        for row in self.row_headers:
            self._data[row] = {}
            for col in self.column_headers:
                self._data[row][col] = ''

    # Some methods need to be reimplemented when we subclass QAbstractTableModel
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.row_headers)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.column_headers)

    def headerData(self, section, orientation, role):
        if role != QtCore.Qt.DisplayRole:
            return None
        if orientation == QtCore.Qt.Horizontal:
            return self.column_headers[section]
        else:
            return self.row_headers[section]

    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        if index.isValid() and role == QtCore.Qt.DisplayRole:
            row = index.row()
            col = index.column()
            item_text = self._data[self.row_headers[row]][self.column_headers[col]]
            return item_text

    def addItem(self, row, col, new_data):
        if row > self.rowCount() or col > self.columnCount():
            return
        row_idx = self.row_headers[row]
        col_idx = self.column_headers[col]
        self._data[row_idx][col_idx] = new_data
        index = self.createIndex(row, col, QtCore.QModelIndex())
        self.dataChanged.emit(index, QtCore.Qt.DisplayRole)

class Worker(QtCore.QThread):
    # This signals will be emitted during the processing.
    # The first one, updateProfress includes an int as argument
    # It lets the signal know to expect an integer argument when emitting.
    updateProgress = QtCore.Signal(int)
    # The second one, addItem includes two ints and a generic object as
    # the argument. The object can be any python object, like a dictionary
    addItem = QtCore.Signal(int, int, object)

    #You can do any extra things in this init you need, but for this example
    #nothing else needs to be done expect call the super's init
    def __init__(self, data):
        QtCore.QThread.__init__(self)
        self.data = data

    #A QThread is run by calling it's start() function, which calls this run()
    #function in it's own "thread". 
    def run(self):
        percent_inc = 100.0/(len(self.data['cols'])*len(self.data['rows']))
        percent = 0.0
        for col in self.data['cols']:
            for row in self.data['rows']:
                #Emit the signal so it can be received on the UI side.
                item_text = "{}{}".format(col, row)
                row_idx = self.data['rows'].index(row)
                col_idx = self.data['cols'].index(col)
                self.addItem.emit(row_idx, col_idx, item_text)
                percent += percent_inc
                self.updateProgress.emit(percent)
                time.sleep(.25)
        self.updateProgress.emit(100)


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()