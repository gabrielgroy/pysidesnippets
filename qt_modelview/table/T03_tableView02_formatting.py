'''
Simple Table View example
    It shows how to create a simple list view using QTableView and QAbstractTableModel
'''
import os
import sys
from Qt import QtCore, QtWidgets, QtGui

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E', 'F'],
            'rows': [1, 2, 3]
        }

        # Create table, using the Model/View framework
        table_model = MyTableModel(self.data, self)
        table_view = QtWidgets.QTableView()
        table_view.setIconSize(QtCore.QSize(50,50))
        
        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(table_view)
        self.setLayout(layout)

        # Connect the view with the model
        table_view.setModel(table_model)
        # Row height need to be set after filling in the data
        table_view.setRowHeight(0, 100)

class MyTableModel(QtCore.QAbstractTableModel):
    def __init__(self, data, parent=None, *args):
        """ This model will combine row and column in each cell
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.row_headers = data['rows']
        self.column_headers = data['cols']

    # Some methods need to be reimplemented when we subclass QAbstractTableModel
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.row_headers)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.column_headers)

    def headerData(self, section, orientation, role):
        if role != QtCore.Qt.DisplayRole:
            return None
        if orientation == QtCore.Qt.Horizontal:
            return self.column_headers[section]
        else:
            return self.row_headers[section]
        
    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        if not index.isValid():
            return None
        if role == QtCore.Qt.DisplayRole:
            item_text = "{}{}".format(self.column_headers[index.column()], self.row_headers[index.row()])
            return item_text
        elif index.row()==0 and role == QtCore.Qt.DecorationRole:
            return QtGui.QIcon(icon_boat)
        elif index.row()==1 and role == QtCore.Qt.ForegroundRole:
            return QtGui.QColor('red') # or QtGui.QColor(QtCore.Qt.red)
        elif index.column()==1 and role == QtCore.Qt.BackgroundRole:
            return QtGui.QColor('yellow') # or QtGui.QColor(QtCore.Qt.yellow)
        elif index.row()==2 and role == QtCore.Qt.FontRole:
            return QtGui.QFont("Times", 20, QtGui.QFont.Bold)
        elif index.row()==2 and role == QtCore.Qt.TextAlignmentRole:
                return QtCore.Qt.AlignRight
        else:
            return None


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()