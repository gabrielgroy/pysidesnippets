'''
Simple Table View example
    It shows how to create a simple list view using QTableView and QAbstractTableModel
'''
import sys
from Qt import QtCore, QtWidgets, QtGui

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E', 'F'],
            'rows': [1, 2, 3]
        }

        # Create table, using the Model/View framework
        table_model = MyTableModel(self.data, self)
        table_view = QtWidgets.QTableView()

        # Connect the view with the model
        table_view.setModel(table_model)
        
        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(table_view)
        self.setLayout(layout)

class MyTableModel(QtCore.QAbstractTableModel):
    def __init__(self, data, parent=None, *args):
        """ This model will combine row and column in each cell
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.row_headers = data['rows']
        self.column_headers = data['cols']

    # Some methods need to be reimplemented when we subclass QAbstractTableModel
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.row_headers)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.column_headers)

    def headerData(self, section, orientation, role):
        if role != QtCore.Qt.DisplayRole:
            return None
        if orientation == QtCore.Qt.Horizontal:
            return self.column_headers[section]
        else:
            return self.row_headers[section]
        
    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        if index.isValid() and role == QtCore.Qt.DisplayRole:
            item_text = "{}{}".format(self.column_headers[index.column()], self.row_headers[index.row()])
            return item_text
        else:
            return None


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()