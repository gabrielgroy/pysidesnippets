'''
Simple Table View example - Editing
    To enable editing of the items, we need to reimplement the methods 'flags' and 'setData'
    The returned flags need to include QtCore.Qt.ItemIsEditable
    'setData' is in charge of storing the edited data in the model
'''
import sys
from Qt import QtCore, QtWidgets, QtGui

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E', 'F'],
            'rows': [1, 2, 3]
        }

        # Create table, using the Model/View framework
        table_model = MyTableModel(self.data, self)
        table_view = QtWidgets.QTableView()

        # Connect the view with the model
        table_view.setModel(table_model)
        
        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(table_view)
        self.setLayout(layout)

class MyTableModel(QtCore.QAbstractTableModel):
    def __init__(self, data, parent=None, *args):
        """ This model will combine row and column in each cell
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.row_headers = data['rows']
        self.column_headers = data['cols']

        self._data = {}
        for row in range(len(self.row_headers)):
            self._data[row] = {}
            for col in range(len(self.column_headers)):
                item_text = "{}{}".format(data['cols'][col], data['rows'][row])
                self._data[row][col] = item_text

    # Some methods need to be reimplemented when we subclass QAbstractTableModel
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.row_headers)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.column_headers)

    def headerData(self, section, orientation, role):
        if role != QtCore.Qt.DisplayRole:
            return None
        if orientation == QtCore.Qt.Horizontal:
            return self.column_headers[section]
        else:
            return self.row_headers[section]

    # When we double click an item to edit, the flags function is called to check
    # if the item has the ItemIsEditable flag or not. If not, we won't be able to edit
    def flags(self, index):
        if not index.isValid():
            return None
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable

    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        if index.isValid() and role == QtCore.Qt.DisplayRole:
            return self._data[index.row()][index.column()]
            # item_text = "{}{}".format(self.column_headers[index.column()], self.row_headers[index.row()])
            # return item_text
        else:
            return None

    # To actually edit the data, this method needs to be reimplemented
    def setData(self, index, value, role):
        # The index needs to be valid
        if not index.isValid():
            return False
        # and the role has to be EditRole
        if role != QtCore.Qt.EditRole:
            return False
        # From the index we get the info we need to edit the underlying data
        row = index.row()
        col = index.column()
        self._data[row][col] = value
        # After modifying the data we have to explicitly emit the signal dataChanged
        # The first arg is the topleft index and the second arg the bottomright index
        # In this case, they're the same
        self.dataChanged.emit(index, index)
        return True

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()