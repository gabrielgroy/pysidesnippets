'''
Simple Table Widget example
    It shows how to create a simple list view using QTableWidget
'''
import os
import sys
from Qt import QtWidgets, QtGui, QtCore

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")

ICON_SIZE = 200

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C'],
            'rows': [1, 2]
        }

        # Create Table Widget
        self.tableWidget = QtWidgets.QTableWidget(self)
        self.tableWidget.setRowCount(len(self.data['rows']))
        self.tableWidget.setColumnCount(len(self.data['cols']))
        self.tableWidget.setHorizontalHeaderLabels(self.data['cols'])
        self.tableWidget.setIconSize(QtCore.QSize(ICON_SIZE,ICON_SIZE))

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tableWidget)
        self.setLayout(layout)

        # Fill process
        for row, row_name in enumerate(self.data['rows']):
            for col, col_name in enumerate(self.data['cols']):
                item_text = "{}{}".format(col_name, row_name)
                item = QtWidgets.QTableWidgetItem(item_text)

                # Format
                if row == 0:
                    item.setIcon(QtGui.QIcon(icon_boat))
                elif row == 1:
                    item.setText('')

                self.tableWidget.setItem(row, col, item)

                if row ==1:
                    # We get the index from the item
                    index = self.tableWidget.indexFromItem(item)
                    # We now create the label with the image
                    icon_pixmap = QtGui.QPixmap(icon_boat)#.scaledToHeight(ICON_SIZE)
                    label = QtWidgets.QLabel(self)
                    label.setPixmap(icon_pixmap)
                    label.setAlignment(QtCore.Qt.AlignCenter)
                    label.setContentsMargins(0,0,0,0)
                    label.setAutoFillBackground(False)
                    # And finally we add the widget to the item/index
                    self.tableWidget.setIndexWidget(index, label)


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()