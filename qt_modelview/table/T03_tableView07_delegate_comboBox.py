'''
Simple Table View example - Editing
    To enable editing of the items, we need to reimplement the methods 'flags' and 'setData'
    The returned flags need to include QtCore.Qt.ItemIsEditable
    'setData' is in charge of storing the edited data in the model
'''
import sys
from Qt import QtCore, QtWidgets, QtGui

DEFAULT_VALUE = 'A'

class ComboBoxDelegate(QtWidgets.QStyledItemDelegate):
    def createEditor(self, parent, option, index):
        editor = QtWidgets.QComboBox(parent)
        editor.addItems(['A', 'B', 'C'])
        editor.currentIndexChanged.connect(self.emitCommitData)
        return editor

    def setEditorData(self, editor, index):
        value = index.model().data(index, QtCore.Qt.EditRole)
        pos = editor.findText(value, QtCore.Qt.MatchExactly)
        editor.setCurrentIndex(pos)

    def setModelData(self, editor, model, index):
        value = editor.currentText()
        model.setData(index, value, QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        '''
        It is the responsibility of the delegate to manage the editor's geometry.
        The geometry must be set when the editor is created, and when the item's size or position
        in the view is changed. Fortunately, the view provides all the necessary geometry information
        inside a view 'option' object.
        '''
        editor.setGeometry(option.rect)

    def emitCommitData(self):
        self.commitData.emit(self.sender())

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E', 'F'],
            'rows': [1, 2, 3]
        }

        # Create table, using the Model/View framework
        self.table_model = MyTableModel(self.data, self)
        self.table_view = QtWidgets.QTableView()

        # Connect the view with the model
        self.table_view.setModel(self.table_model)
        self.table_view.setItemDelegate(ComboBoxDelegate())
        
        # # Optional. To permanently expose the delegate editor in each cell
        # for row in range(len(self.data['rows'])):
        #     for col in range(len(self.data['cols'])):
        #         index = self.table_model.index(row, col)
        #         self.table_view.openPersistentEditor(index)

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to print data")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.table_view)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        self.pushButton.clicked.connect(self.printData)
    
    def printData(self):
        print(self.table_model._data)

class MyTableModel(QtCore.QAbstractTableModel):
    def __init__(self, data, parent=None, *args):
        """ This model will combine row and column in each cell
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.row_headers = data['rows']
        self.column_headers = data['cols']

        self._data = {}
        for row in range(len(self.row_headers)):
            self._data[row] = {}
            for col in range(len(self.column_headers)):
                self._data[row][col] = DEFAULT_VALUE

    # Some methods need to be reimplemented when we subclass QAbstractTableModel
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.row_headers)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.column_headers)

    def headerData(self, section, orientation, role):
        if role != QtCore.Qt.DisplayRole:
            return None
        if orientation == QtCore.Qt.Horizontal:
            return self.column_headers[section]
        else:
            return self.row_headers[section]

    # When we double click an item to edit, the flags function is called to check
    # if the item has the ItemIsEditable flag or not. If not, we won't be able to edit
    def flags(self, index):
        if not index.isValid():
            return None
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable

    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        if index.isValid() and role in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):
            return self._data[index.row()][index.column()]
            # item_text = "{}{}".format(self.column_headers[index.column()], self.row_headers[index.row()])
            # return item_text
        else:
            return None

    # To actually edit the data, this method needs to be reimplemented
    def setData(self, index, value, role):
        # The index needs to be valid
        if not index.isValid():
            return False
        # and the role has to be EditRole
        if role != QtCore.Qt.EditRole:
            return False
        # From the index we get the info we need to edit the underlying data
        row = index.row()
        col = index.column()
        self._data[row][col] = value
        # After modifying the data we have to explicitly emit the signal dataChanged
        # The first arg is the topleft index and the second arg the bottomright index
        # In this case, they're the same
        self.dataChanged.emit(index, index)
        return True

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()