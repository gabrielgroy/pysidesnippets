'''
Simple Table View example
    It shows how to create a simple list view using QTableView and QAbstractTableModel
'''
import sys
from Qt import QtCore, QtWidgets, QtGui

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E'],
            'rows': [1, 2, 3]
        }

        # Create table, using the Model/View framework
        table_model = QtGui.QStandardItemModel()
        table_view = QtWidgets.QTableView()
        table_view.setIconSize(QtCore.QSize(100, 100))
        
        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(table_view)
        self.setLayout(layout)

        # Connect the view with the model
        table_view.setModel(table_model)

        # Fill data
        table_model.setColumnCount(len(self.data['cols']))
        table_model.setRowCount(len(self.data['rows']))

        for i,text in enumerate(self.data['cols']):
            item = QtGui.QStandardItem(text)
            table_model.setHorizontalHeaderItem(i, item)

        for row, row_name in enumerate(self.data['rows']):
            for col, col_name in enumerate(self.data['cols']):
                text = "{}{}".format(col_name, row_name)
                item = QtGui.QStandardItem(text)
                item.setFlags(\
                    QtCore.Qt.ItemIsEnabled\
                    | QtCore.Qt.ItemIsEditable\
                    # | QtCore.Qt.ItemIsSelectable\
                    # | QtCore.Qt.ItemIsDragEnabled\
                    # | QtCore.Qt.ItemIsDropEnabled\
                    # | QtCore.Qt.ItemIsUserCheckable\
                    )
                table_model.setItem(row, col, item)
                

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()