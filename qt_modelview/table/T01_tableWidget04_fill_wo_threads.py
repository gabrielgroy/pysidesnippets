'''
Simple Table Widget example
    It shows how to create a simple table view using QTableWidget,
    triggering the fill of the table with a button
'''
import sys
import time
from Qt import QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E'],
            'rows': [1, 2, 3]
        }

        # Create Table Widget
        self.tableWidget = QtWidgets.QTableWidget(self)
        self.tableWidget.setRowCount(len(self.data['rows']))
        self.tableWidget.setColumnCount(len(self.data['cols']))
        self.tableWidget.setHorizontalHeaderLabels(self.data['cols'])

        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tableWidget)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        # Connections
        self.pushButton.clicked.connect(self.runFillProcess)

    def runFillProcess(self):
        percent_inc = 100.0/(len(self.data['cols'])*len(self.data['rows']))
        percent = 0.0
        for row, row_name in enumerate(self.data['rows']):
            for col, col_name in enumerate(self.data['cols']):
                item_text = "{}{}".format(col_name, row_name)
                item = QtWidgets.QTableWidgetItem(item_text)
                self.tableWidget.setItem(row, col, item)
                percent += percent_inc
                self.progressBar.setValue(int(percent))
                time.sleep(.25)
        self.progressBar.setValue(100)



def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()