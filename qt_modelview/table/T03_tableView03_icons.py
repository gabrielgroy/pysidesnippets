'''
Simple Table View example
    It shows how to create a simple list view using QTableView and QAbstractTableModel
'''
import os
import sys
from Qt import QtCore, QtWidgets, QtGui

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E', 'F'],
            'rows': [1, 2]
        }

        # Create table, using the Model/View framework
        table_model = MyTableModel(self.data, self)
        table_view = QtWidgets.QTableView()
        table_view.setIconSize(QtCore.QSize(50,50))
        
        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(table_view)
        self.setLayout(layout)

        # Connect the view with the model
        table_view.setModel(table_model)

        # To set a widget in the cells:
        for col, col_name in enumerate(self.data['cols']):
            # We get the index from the item
            index = table_model.index(1, col, QtCore.QModelIndex())
            # We now create the label with the image
            icon_pixmap = QtGui.QPixmap(icon_boat)#.scaledToHeight(ICON_SIZE)
            label = QtWidgets.QLabel(self)
            label.setPixmap(icon_pixmap)
            label.setAlignment(QtCore.Qt.AlignCenter)
            label.setContentsMargins(0,0,0,0)
            label.setAutoFillBackground(False)
            # And finally we add the widget to the item/index
            table_view.setIndexWidget(index, label)
        
        # Row height need to be set after filling in the data
        table_view.setRowHeight(0, 50)
        table_view.setRowHeight(1, 70)

class MyTableModel(QtCore.QAbstractTableModel):
    def __init__(self, data, parent=None, *args):
        """ This model will combine row and column in each cell
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.row_headers = data['rows']
        self.column_headers = data['cols']

    # Some methods need to be reimplemented when we subclass QAbstractTableModel
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.row_headers)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.column_headers)

    def headerData(self, section, orientation, role):
        if role != QtCore.Qt.DisplayRole:
            return None
        if orientation == QtCore.Qt.Horizontal:
            return self.column_headers[section]
        else:
            return self.row_headers[section]
        
    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        if not index.isValid():
            return None
        if role == QtCore.Qt.DisplayRole:
            return ''
            # item_text = "{}{}".format(self.column_headers[index.column()], self.row_headers[index.row()])
            # return item_text
        elif index.row()==0 and role == QtCore.Qt.DecorationRole:
            return QtGui.QIcon(icon_boat)
        else:
            return None


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()