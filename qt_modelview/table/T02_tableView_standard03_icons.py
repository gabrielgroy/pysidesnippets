'''
Simple Table View example
    It shows how to create a simple list view using QTableView and QAbstractTableModel
'''
import os
import sys
from Qt import QtCore, QtWidgets, QtGui

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")

ICON_SIZE = 100

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E'],
            'rows': [1, 2]
        }

        # Create table, using the Model/View framework
        table_model = QtGui.QStandardItemModel()
        table_view = QtWidgets.QTableView()
        table_view.setIconSize(QtCore.QSize(100, 100))
        
        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(table_view)
        self.setLayout(layout)

        # Connect the view with the model
        table_view.setModel(table_model)

        # Fill data
        table_model.setColumnCount(len(self.data['cols']))
        table_model.setRowCount(len(self.data['rows']))

        for i,text in enumerate(self.data['cols']):
            item = QtGui.QStandardItem(text)
            table_model.setHorizontalHeaderItem(i, item)

        for row, row_name in enumerate(self.data['rows']):
            for col, col_name in enumerate(self.data['cols']):

                # Format
                if row == 0:
                    text = "{}{}".format(col_name, row_name)
                    item = QtGui.QStandardItem(text)
                    ## Icon in the cell in the standard way
                    item.setIcon(QtGui.QIcon(icon_boat))
                    table_model.setItem(row, col, item)
                elif row == 1:
                    ## Icon in the cell through a widget
                    item = QtGui.QStandardItem()
                    item.setSizeHint(QtCore.QSize(ICON_SIZE,ICON_SIZE))
                    table_model.setItem(row, col, item)

                    # We get the index from the item
                    index = table_model.index(row, col, QtCore.QModelIndex())
                    # We now create the label with the image
                    icon_pixmap = QtGui.QPixmap(icon_boat)#.scaledToHeight(ICON_SIZE)
                    label = QtWidgets.QLabel(self)
                    label.setPixmap(icon_pixmap)
                    label.setAlignment(QtCore.Qt.AlignCenter)
                    label.setContentsMargins(0,0,0,0)
                    label.setAutoFillBackground(False)
                    # And finally we add the widget to the item/index
                    table_view.setIndexWidget(index, label)

        # Row height need to be set after filling in the data
        table_view.setRowHeight(0, 100)
        table_view.setRowHeight(1, 100)

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()