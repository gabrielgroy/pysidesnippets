'''
Simple Table View example
    It shows how to create a simple list view using QTableView and QAbstractTableModel
'''
import os
import sys
from Qt import QtCore, QtWidgets, QtGui

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(800, 300)

        self.data = {
            'cols': ['A', 'B', 'C', 'D', 'E'],
            'rows': [1, 2, 3]
        }

        # Create table, using the Model/View framework
        table_model = QtGui.QStandardItemModel()
        table_view = QtWidgets.QTableView()
        table_view.setIconSize(QtCore.QSize(100, 100))
        
        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(table_view)
        self.setLayout(layout)

        # Connect the view with the model
        table_view.setModel(table_model)

        # Fill data
        table_model.setColumnCount(len(self.data['cols']))
        table_model.setRowCount(len(self.data['rows']))

        for i,text in enumerate(self.data['cols']):
            item = QtGui.QStandardItem(text)
            table_model.setHorizontalHeaderItem(i, item)

        for row, row_name in enumerate(self.data['rows']):
            for col, col_name in enumerate(self.data['cols']):
                text = "{}{}".format(col_name, row_name)
                item = QtGui.QStandardItem(text)

                # Format
                if row == 0:
                    item.setIcon(QtGui.QIcon(icon_boat))
                elif row == 1:
                    item.setForeground(QtGui.QColor('red')) # or QtGui.QColor(QtCore.Qt.red)
                elif row == 2:
                    item.setTextAlignment(QtCore.Qt.AlignRight)
                    item.setFont(QtGui.QFont("Times", 20, QtGui.QFont.Bold))
                if col == 1:
                    item.setBackground(QtGui.QColor('yellow'))

                table_model.setItem(row, col, item)

        # Row height need to be set after filling in the data
        table_view.setRowHeight(0, 100)

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()