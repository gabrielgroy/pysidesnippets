'''
Simple Tree Widget example
    It shows how to create a simple list view using QTreeWidget,
    triggering the fill of the tree with a button
'''
import sys
import time
from Qt import QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(400, 500)

        # Tree data
        self.data = {
            'headers': ['Item', 'Description'],
            'tree': {
                'A': {('A1', 'Item A1'), ('A2', 'Item A2'), ('A3', 'Item A3'), },
                'B': {('B1', 'Item B1'), ('B2', 'Item B2'), ('B3', 'Item B3'), },
                'C': {('C1', 'Item C1'), ('C2', 'Item C2'), ('C3', 'Item C3'), },
            }
        }

        # Tree Widget
        self.tree_widget = QtWidgets.QTreeWidget(self)

        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_widget)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        # Connections
        self.pushButton.clicked.connect(self.runFillProcess)

    def runFillProcess(self):
        self.tree_widget.setColumnCount(len(self.data['headers']))
        self.tree_widget.setHeaderLabels(self.data['headers'])

        items_amount = 0
        for row in self.data['tree']:
            items_amount += len(self.data['tree'][row])
        percent_inc = 100.0/items_amount
        percent = 0.0
        
        root = QtWidgets.QTreeWidgetItem(self.tree_widget)
        for row in sorted(self.data['tree']):
            item = QtWidgets.QTreeWidgetItem(root, [row,])
            for subitem_text, subitem_description in self.data['tree'][row]:
                subitem = QtWidgets.QTreeWidgetItem(item, [subitem_text, subitem_description])
                percent += percent_inc
                self.progressBar.setValue(int(percent))
                time.sleep(.25)
            self.tree_widget.expandItem(item)
        self.tree_widget.expandItem(root)
        
def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()