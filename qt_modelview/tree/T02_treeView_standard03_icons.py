'''
Simple Tree View example
    It shows how to create a simple list view using QTreeView and QStandardItemModel

    Notice that with QStandardItemModel, we cannot rename the headers
'''
import os
import sys
from Qt import QtCore, QtWidgets, QtGui

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")

ICON_SIZE = 100

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(500, 700)

        # Tree data
        self.data = {
            'headers': ['Item', 'Standard Icon', 'Widget Icon'],
            'tree': {
                'A': (('A1', 'Item A1', ''), ('A2', 'Item A2', ''), ('A3', 'Item A3', ''), ),
                'B': (('B1', 'Item B1', ''), ('B2', 'Item B2', ''), ('B3', 'Item B3', ''), ),
                'C': (('C1', 'Item C1', ''), ('C2', 'Item C2', ''), ('C3', 'Item C3', ''), ),
            }
        }

        # Create table, using the Model/View framework
        self.tree_view = QtWidgets.QTreeView()
        self.tree_model = QtGui.QStandardItemModel()
        self.tree_view.setModel(self.tree_model)
        self.tree_view.setIconSize(QtCore.QSize(ICON_SIZE,ICON_SIZE))


        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_view)
        self.setLayout(layout)

        # Fill View

        # Header
        for i,text in enumerate(self.data['headers']):
            item = QtGui.QStandardItem(text)
            self.tree_model.setHorizontalHeaderItem(i, item)

        # Tree
        rootNode = self.tree_model.invisibleRootItem()
        rootNode.setColumnCount(len(self.data['headers']))
        for row, row_name in enumerate(sorted(self.data['tree'])):
            item = QtGui.QStandardItem(row_name)
            item.setRowCount(len(self.data['tree'][row_name]))
            for i, item_data in enumerate(self.data['tree'][row_name]):
                subitem_name = QtGui.QStandardItem(item_data[0])
                item.setChild(i, 0, subitem_name)

                # Standard icon
                subitem_icon1 = QtGui.QStandardItem(item_data[1])
                subitem_icon1.setIcon(QtGui.QIcon(icon_boat))
                subitem_icon1.setTextAlignment(QtCore.Qt.AlignRight)
                subitem_icon1.setBackground(QtGui.QColor('lightgray'))
                item.setChild(i, 1, subitem_icon1)
                self.tree_view.setColumnWidth(1, 180)

                # Custom icon
                subitem_icon2 = QtGui.QStandardItem(item_data[2])
                subitem_icon2.setBackground(QtGui.QColor('gray'))

                icon_pixmap = QtGui.QPixmap(icon_boat)#.scaledToHeight(ICON_SIZE)
                label = QtWidgets.QLabel(self)
                label.setPixmap(icon_pixmap)
                label.setAlignment(QtCore.Qt.AlignCenter)
                label.setContentsMargins(0,0,0,0)
                label.setAutoFillBackground(False)

                item.setChild(i, 2, subitem_icon2)
                subitem_index = self.tree_model.indexFromItem(subitem_icon2)
                self.tree_view.setIndexWidget(subitem_index, label)


            rootNode.appendRow(item)
        # Let's expand the tree
        self.tree_view.expandAll()
        self.tree_view.setColumnWidth(0, 80)


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()