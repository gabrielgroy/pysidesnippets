'''
Simple Tree View example - Files

    The standard way of populating the tree model is creating all the items in a
    series of loops.
    But if that load process needs to take too much time, we may want to do this in a lazy way.
    For example: if we want a tree view that displays the file system, we don't want to preload
    and create an item per file and folder in the file system, as there might be hundreds of
    thousands of files, and that preload would take forever. Instead, we want to populate the
    folders as we expand them.

    This script is a first approach, using the file system tree view as the example.

    Instead of having a data variable with all the items, we'll need an item per file and dir.

    In MyTreeModel, in the __init__ we're only creating the self.rootItem item instead
    of launching a complete populate process (that would be slow, depending on the disk contents).
    Then, in the MyItem class, in the __init__ function, we're setting self.childItems to None
    The goal is to defer the creation of the child items until they're requested by the view.
    So we create the 'populateChildren' function to query the file system and create the child
    items when the children are requested.
    Then, in several functions, like 'childCount', as we need to provide an answer, we check if the
    children are already calculated or not, and if they're not, we create them:
        if self.childItems is None:
            self.populateChildren()

    The view is going to query the children for the root item and the immediate children, to know if
    displaying the expansion arrow or not, but not below.
'''
import os
import sys
from Qt import QtCore, QtWidgets, QtGui

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(400, 500)

        # Tree data
        self.root_path = "D:\\"

        # Create table, using the Model/View framework
        self.tree_view = QtWidgets.QTreeView()
        
        # Connect the view with the model
        self.tree_model = MyTreeModel(self.root_path, self)
        self.tree_view.setModel(self.tree_model)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_view)
        self.setLayout(layout)

class MyTreeModel(QtCore.QAbstractItemModel):
    '''
        Our model is a subclass of QtCore.QAbstractItemModel
        We cannot use abstract classes directly. We always need to subclass them
        and reimplement some methods
        
        In the same way, for our items, we usually will need to define a custom class.
        
        The model needs to work with the view or views, because we can have multiple views
        for the same model.
        The model stores the data and communicates with the view through indexes.
        Indexes and items in the model are two completely different things, but they're related.
        Indexes are temporary objects used for the communication and expire aftwerwards.
        
        It is responsability of the model to build the indexes and relate them with the items.
        
        All of these methods need to be reimplemented.
        
        The view doesn't create the indexes on its own. It asks the model to create them.
        The three methods in the model that build indexes are:
        - rootIndex, to build the index for the root item
        - index, to build indexes for items that live inside other items
        - parent, to build the index that is the parent of another index
        
        The methods with which the view asks the model about the data are:
        - rowCount
        - columnCount
        - headerData
        - data
    '''
    def __init__(self, root_path, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        self.root_path = root_path
        self.headers = ['Name',]
        self.rootItem = MyItem(self.root_path)

    # Data query methods. With these, the view asks for data to the model
    # passing an index
    def rowCount(self, parent=QtCore.QModelIndex()):
        '''
        The view will ask the model about the number of rows of a index calling
        this method, passing the item index as the parent argument
        '''
        if parent.column() > 0:
            return 0
        # We need to identify the item referenced by the passed index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        return parentItem.childCount()

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.headers)

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.headers[section]
        
    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        ''' Give me the data of the item of this index in these coordinates
            The view will query the model about the data of an item for a given role
            passing an index that contains the coordinates
            It passes an index, because the view doesn't have direct access to the items
            It is the model who identifies the item that corresponds to the index,
            and gets the piece of data that corresponds to the role
            We can think about the role as the key of a dictionary stored by the item
        '''
        # As always, if the index is not valid, it's because it refers to the root item
        if not index.isValid():
            return None

        # As we may not be using all the possible roles, we filter to consider only the ones
        # we care about
        elif not role in (QtCore.Qt.DisplayRole, QtCore.Qt.ForegroundRole): # QtCore.Qt.DecorationRole, QtCore.Qt.UserRole, QtCore.Qt.ForegroundRole):
            return None

        # Once we pass the filter, we get the item, ask for the data of the role and return it
        item = index.internalPointer()
        if role == QtCore.Qt.DisplayRole:
            return item.data(index.column())
        elif role == QtCore.Qt.ForegroundRole:
            if item.isdir:
                return QtGui.QColor('blue')

    # Index building methos. With these, the view asks the model to build indexes
    # based on row, column, and parent
    def rootIndex(self):
        ''' Metodo necesario, porque lo pide el view '''
        return self.createIndex(0, 0, self.rootItem)

    def index(self, row, column, parent=QtCore.QModelIndex()):
        ''' Create an index with the item of the coordinates row and column under parent

            With this method, the view asks the model for the data for a cell
            First it asks the model to build an index for a row and column, under another
            index identified as 'parent'. Then later, this index will be what the view will
            pass to the model to ask for the data of the index/item
        '''
        # The first stel is to check if the passed args are valid
        # The model should tell us if an index is possible or not for the row, col and parent
        if not self.hasIndex(row, column, parent):
            # If it is not possible , the model returns an empty invalid index
            return QtCore.QModelIndex()
        
        # To build a valid index, we need an item
        # Let's identify the item referenced by the parent index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
            
        # Once we have the parent index, we get the child item
        # For this particular example we only need the row
        childItem = parentItem.child(row)
        if childItem:
            # If the item exists, we can create and return the index
            index = self.createIndex(row, column, childItem)
            return index
        else:
            # If not, we return and empty invalud index
            return QtCore.QModelIndex()

    def parent(self, index):
        '''
            In this method, starting from the passed index, we have to identify the item
            and build an index for its parent
        '''
        if not index.isValid():
            return QtCore.QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QtCore.QModelIndex()

        return self.createIndex(childItem.row(), 0, parentItem)

class MyItem(object):
    def __init__(self, path, parent=None):
        print("Item init: {}".format(path))
        self.parentItem = parent
        self.path = path
        self.childItems = None
        self.isdir = False
        if not os.path.exists(self.path):
            print("Path doesn't exist: '{}'".format(self.path))
        if os.path.isdir(self.path):
            self.isdir = True
    
    def populateChildren(self):
        if self.childItems is not None:
            return
        if not self.isdir:
            self.childItems = []
            return
        contents = os.listdir(self.path)
        self.childItems = []
        for filename in contents:
            filepath = os.path.join(self.path, filename)
            item = MyItem(filepath, self)
            self.childItems.append(item)
        print("-------------------------------------------------------")

    def appendChild(self, item):
        ''' Method to add children to this item '''
        if self.childItems is None:
            self.populateChildren()
        self.childItems.append(item)

    def child(self, row):
        ''' Mathod to access a child '''
        if self.childItems is None:
            self.populateChildren()
        return self.childItems[row]

    def childCount(self):
        ''' Returns the children amount '''
        if self.childItems is None:
            self.populateChildren()
        return len(self.childItems)

    def columnCount(self):
        ''' Returns the columns amount '''
        return 1

    def data(self, column):
        '''
        Method to access the item's data.
        The view will query the model, and the model will wuery the item
        Args:
        - column
            0 is the name, 1 the description
        '''
        if column > 0:
            return None
        return os.path.basename(self.path)

    def parent(self):
        ''' Returns a reference to the parent '''
        return self.parentItem

    def row(self):
        '''
        Returns the row number in which this item is placed
        under the children of the parent item
        '''
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()