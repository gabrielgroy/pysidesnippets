'''
Simple Tree View example
    It shows how to create a simple list view using QTreeView and QAbstractItemModel,
    triggering the fill of the tree with a button, and filling
    it from another thread.

    Not sure if I can query the model from the thread to build indexes
    so I added a mutex just in case
'''
import sys
import time
from Qt import QtCore, QtWidgets

MUTEX = QtCore.QMutex()

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(400, 500)

        # Tree data
        self.data = {
            'headers': ['Item', 'Description'],
            'tree': {
                'A': [('A1', 'Item A1'), ('A2', 'Item A2'), ('A3', 'Item A3'), ],
                'B': [('B1', 'Item B1'), ('B2', 'Item B2'), ('B3', 'Item B3'), ],
                'C': [('C1', 'Item C1'), ('C2', 'Item C2'), ('C3', 'Item C3'), ],
            }
        }

        # Create table, using the Model/View framework
        self.tree_view = QtWidgets.QTreeView()
        
        # Connect the view with the model
        self.tree_model = MyTreeModel(self.data, self)
        self.tree_view.setModel(self.tree_model)
        self.tree_view.expandAll()

        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_view)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        self.worker = Worker(self.tree_model, self.data)
        self.worker.updateProgress.connect(self.progressBar.setValue)
        self.worker.addItem.connect(self.tree_model.addItem)
        self.worker.finished.connect(self.tree_view.expandAll)
        self.tree_model.layoutChanged.connect(self.tree_view.expandAll)
        self.pushButton.clicked.connect(self.worker.start)

class MyTreeModel(QtCore.QAbstractItemModel):
    '''
    '''
    def __init__(self, data=None, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        self.headers = []
        self._data = {}
        if data:
            self.headers = data['headers']
            self._data = data['tree']
        self.rootItem = MyItem()

    # Data query methods. With these, the view asks for data to the model
    # passing an index
    def rowCount(self, parent=QtCore.QModelIndex()):
        '''
        The view will ask the model about the number of rows of a index calling
        this method, passing the item index as the parent argument
        '''
        if parent.column() > 0:
            return 0
        # We need to identify the item referenced by the passed index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        return parentItem.childCount()

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.headers)

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.headers[section]
        
    def data(self, index, role):
        ''' Give me the data of the item of this index in these coordinates
            The view will query the model about the data of an item for a given role
            passing an index that contains the coordinates
            It passes an index, because the view doesn't have direct access to the items
            It is the model who identifies the item that corresponds to the index,
            and gets the piece of data that corresponds to the role
            We can think about the role as the key of a dictionary stored by the item
        '''
        # As always, if the index is not valid, it's because it refers to the root item
        if not index.isValid():
            return None

        # As we may not be using all the possible roles, we filter to consider only the ones
        # we care about
        elif not role in (QtCore.Qt.DisplayRole, ): # QtCore.Qt.DecorationRole, QtCore.Qt.UserRole, QtCore.Qt.ForegroundRole):
            return None

        # Once we pass the filter, we get the item, ask for the data of the role and return it
        item = index.internalPointer()
        if role == QtCore.Qt.DisplayRole:
            return item.data(index.column())

    # This method is only necessary if we need to edit the contents
    def setData(self, index, value, role):
        ''' Reimplementing this method we can change data in the model
        '''
        if not index.isValid():
            return None
        elif not role in (QtCore.Qt.DisplayRole, ):
            return None
        item = index.internalPointer()
        if role == QtCore.Qt.DisplayRole:
            item.setData(index.column(), value)
        # To let the view know it has to redraw, we emit the 'dataChanged' signal
        self.dataChanged.emit(index, role)

    # Index building methods. With these, the view asks the model to build indexes
    # based on row, column, and parent
    def rootIndex(self):
        '''
            Metodo necesario, porque lo pide el view
        '''
        return self.createIndex(0, 0, self.rootItem)

    def index(self, row, column, parent=QtCore.QModelIndex()):
        ''' Create an index with the item of the coordinates row and column under parent

            With this method, the view asks the model for the data for a cell
            First it asks the model to build an index for a row and column, under another
            index identified as 'parent'. Then later, this index will be what the view will
            pass to the model to ask for the data of the index/item
        '''
        # The first stel is to check if the passed args are valid
        # The model should tell us if an index is possible or not for the row, col and parent
        if not self.hasIndex(row, column, parent):
            # If it is not possible , the model returns an empty invalid index
            return QtCore.QModelIndex()

        # To build a valid index, we need an item
        # Let's identify the item referenced by the parent index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
            
        # Once we have the parent index, we get the child item
        # For this particular example we only need the row
        childItem = parentItem.child(row)
        if childItem:
            # If the item exists, we can create and return the index
            index = self.createIndex(row, column, childItem)
            return index
        else:
            # If not, we return and empty invalud index
            return QtCore.QModelIndex()

    def parent(self, index):
        '''
            In this method, starting from the passed index, we have to identify the item
            and build an index for its parent
        '''
        if not index.isValid():
            return QtCore.QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QtCore.QModelIndex()

        return self.createIndex(childItem.row(), 0, parentItem)

    # Custom extra methods
    def addItem(self, item_data, parent_index=None):
        ''' Added funtion to edit the model from outside '''
        if (parent_index is None) or (parent_index=='') or (not parent_index.isValid()):
            parent_index = self.rootIndex()
            parent_item = self.rootItem
        else:
            parent_item = parent_index.internalPointer()
        item = MyItem(item_data, parent_item)
        MUTEX.lock()
        self.beginInsertRows(parent_index, parent_item.childCount(), parent_item.childCount()+1)
        parent_item.appendChild(item)
        self.endInsertRows()
        MUTEX.unlock()
        print("Added item: {}".format(item_data))
        self.layoutChanged.emit()
        
class MyItem(object):
    '''
    We create this custom class for the items because we need an object that
    keeps track of it's children and parent, allowing a tree structure
    '''
    def __init__(self, item_data=None, parent=None):
        '''
        At the initialization we store the received data and
        a reference to the parent
        - item_data: dictionary with keys:
            - name
            - description
        - parent: (MyItem) parent item for this item
        '''
        self.parentItem = parent
        if item_data is None:
            item_data = {}
        self.itemData = item_data
        self.childItems = []

    def appendChild(self, item):
        ''' Method to add children to this item '''
        self.childItems.append(item)

    def child(self, row):
        ''' Method to access a child '''
        return self.childItems[row]

    def childCount(self):
        ''' Returns the children amount '''
        return len(self.childItems)

    def columnCount(self):
        ''' Returns the columns amount
        In this examples, it is always two: name and description '''
        return 2

    def data(self, column):
        '''
        Method to access the item's data.
        The view will query the model, and the model will wuery the item
        Args:
        - column
            0 is the name, 1 the description
        '''
        return self.itemData.get({0:'name', 1:'description'}.get(column, ''), '')

    def setData(self, column, value):
        self.itemData[{0:'name', 1:'description'}.get(column)] = value

    def parent(self):
        ''' Returns a reference to the parent '''
        return self.parentItem

    def row(self):
        '''
        Returns the row number in which this item is placed
        under the children of the parent item
        '''
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0

class Worker(QtCore.QThread):

    # This is the signal that will be emitted during the processing.
    # By including int as an argument, it lets the signal know to expect
    # an integer argument when emitting.
    updateProgress = QtCore.Signal(int)
    addItem = QtCore.Signal(object, object)

    # You can do any extra things in this init you need, but for this example
    # nothing else needs to be done expect calling the super's init
    def __init__(self, model, data):
        QtCore.QThread.__init__(self)
        self.model = model
        self.data = data['tree']

    # A QThread is run by calling it's start() function, which calls this run()
    # function in it's own "thread". 
    def run(self):
        items_amount = 0
        for row in self.data:
            items_amount += len(self.data[row])
        percent_inc = 100.0/items_amount
        percent = 0.0

        root_items = sorted(list(self.data))
        for row in range(len(root_items)):
            item_name = root_items[row]
            print(item_name)
            self.addItem.emit({'name': item_name, 'description':''}, None)

            for subitem_text, subitem_description in self.data[item_name]:
                # Not totally sure if this call is thread safe or not, so adding a mutex
                MUTEX.lock()
                item_index = self.model.index(row, 0, self.model.rootIndex())
                MUTEX.unlock()

                self.addItem.emit({'name': subitem_text, 'description':subitem_description}, item_index)
                percent += percent_inc
                self.updateProgress.emit(percent)
                time.sleep(.25)



def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()