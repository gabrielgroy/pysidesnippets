'''
Simple Tree View example - Files

    Second approach to create the tree model items in a lazy way.
    In this one we reimplement the model function 'hasChildren' to avoid creating the
    children until the expand arrow is clicked. But for that, we need to display the
    expansion arrow if the item is a dir.

    Then we also need to trigger the populate process when the expansion arrow is clicked.
'''
import os
import sys
from Qt import QtCore, QtWidgets, QtGui

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(400, 500)

        # Tree data
        self.root_path = "D:\\"

        # Create table, using the Model/View framework
        self.tree_view = QtWidgets.QTreeView()
        
        # Connect the view with the model
        self.tree_model = MyTreeModel(self.root_path, self)
        self.tree_view.setModel(self.tree_model)
        self.tree_view.expanded.connect(self.tree_model.update_model)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_view)
        self.setLayout(layout)

class MyTreeModel(QtCore.QAbstractItemModel):
    def __init__(self, root_path, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        self.root_path = root_path
        self.headers = ['Name',]
        self.rootItem = MyItem(self.root_path)
        self.rootItem.populateChildren()

    def hasChildren(self, index):
        # if not index.isValid():
        #     return False
        item = index.internalPointer()
        if item is None or item.isdir: # and self.data(index, MyTreeModel.ExpandableRole):
            return True
        return super(MyTreeModel, self).hasChildren(index)

    def update_model(self, index):
        if not index.isValid():
            return
        item = index.internalPointer()
        item.populateChildren()
        self.layoutChanged.emit()

    # Data query methods. With these, the view asks for data to the model
    # passing an index
    def rowCount(self, parent=QtCore.QModelIndex()):
        '''
        The view will ask the model about the number of rows of a index calling
        this method, passing the item index as the parent argument
        '''
        if parent.column() > 0:
            return 0
        # We need to identify the item referenced by the passed index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        return parentItem.childCount()

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.headers)

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.headers[section]
        
    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        ''' Give me the data of the item of this index in these coordinates
            The view will query the model about the data of an item for a given role
            passing an index that contains the coordinates
            It passes an index, because the view doesn't have direct access to the items
            It is the model who identifies the item that corresponds to the index,
            and gets the piece of data that corresponds to the role
            We can think about the role as the key of a dictionary stored by the item
        '''
        # As always, if the index is not valid, it's because it refers to the root item
        if not index.isValid():
            return None

        # As we may not be using all the possible roles, we filter to consider only the ones
        # we care about
        elif not role in (QtCore.Qt.DisplayRole, QtCore.Qt.ForegroundRole): # QtCore.Qt.DecorationRole, QtCore.Qt.UserRole, QtCore.Qt.ForegroundRole):
            return None

        # Once we pass the filter, we get the item, ask for the data of the role and return it
        item = index.internalPointer()
        if role == QtCore.Qt.DisplayRole:
            return item.data(index.column())
        elif role == QtCore.Qt.ForegroundRole:
            if item.isdir:
                return QtGui.QColor('blue')

    # Index building methos. With these, the view asks the model to build indexes
    # based on row, column, and parent
    def rootIndex(self):
        ''' Metodo necesario, porque lo pide el view '''
        return self.createIndex(0, 0, self.rootItem)

    def index(self, row, column, parent=QtCore.QModelIndex()):
        ''' Create an index with the item of the coordinates row and column under parent

            With this method, the view asks the model for the data for a cell
            First it asks the model to build an index for a row and column, under another
            index identified as 'parent'. Then later, this index will be what the view will
            pass to the model to ask for the data of the index/item
        '''
        # The first stel is to check if the passed args are valid
        # The model should tell us if an index is possible or not for the row, col and parent
        if not self.hasIndex(row, column, parent):
            # If it is not possible , the model returns an empty invalid index
            return QtCore.QModelIndex()
        
        # To build a valid index, we need an item
        # Let's identify the item referenced by the parent index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
            
        # Once we have the parent index, we get the child item
        # For this particular example we only need the row
        childItem = parentItem.child(row)
        if childItem:
            # If the item exists, we can create and return the index
            index = self.createIndex(row, column, childItem)
            return index
        else:
            # If not, we return and empty invalud index
            return QtCore.QModelIndex()

    def parent(self, index):
        '''
            In this method, starting from the passed index, we have to identify the item
            and build an index for its parent
        '''
        if not index.isValid():
            return QtCore.QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QtCore.QModelIndex()

        return self.createIndex(childItem.row(), 0, parentItem)

class MyItem(object):
    def __init__(self, path, parent=None):
        print("Item init: {}".format(path))
        self.parentItem = parent
        self.path = path
        self.childItems = []
        self.isdir = False
        if not os.path.exists(self.path):
            print("Path doesn't exist: '{}'".format(self.path))
        if os.path.isdir(self.path):
            self.isdir = True
    
    def populateChildren(self):
        if not self.isdir:
            self.childItems = []
            return
        if self.childItems:
            return
        print("Populating -------------------------------------------------------")
        contents = os.listdir(self.path)
        self.childItems = []
        for filename in contents:
            filepath = os.path.join(self.path, filename)
            item = MyItem(filepath, self)
            self.childItems.append(item)
        print("-------------------------------------------------------")

    def appendChild(self, item):
        ''' Method to add children to this item '''
        self.childItems.append(item)

    def child(self, row):
        ''' Mathod to access a child '''
        return self.childItems[row]

    def childCount(self):
        ''' Returns the children amount '''
        return len(self.childItems)

    def columnCount(self):
        ''' Returns the columns amount '''
        return 1

    def data(self, column):
        '''
        Method to access the item's data.
        The view will query the model, and the model will query the item
        Args:
        - column
            0 is the name
        '''
        if column > 0:
            return None
        return os.path.basename(self.path)

    def parent(self):
        ''' Returns a reference to the parent '''
        return self.parentItem

    def row(self):
        '''
        Returns the row number in which this item is placed
        under the children of the parent item
        '''
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()