'''
Simple Tree Widget example
    To add some formatting to the items, we use the functions:
    item.setForeground()
    item.setBackground()
    item.setTextAlignment()
    item.setFont()
    item.setIcon()

    providing the column as the first argument
'''
import os
import sys
from Qt import QtWidgets, QtGui, QtCore

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(400, 500)

        # Tree data
        self.data = {
            'headers': ['Item', 'Description'],
            'tree': {
                'A': (('A1', 'Item A1'), ('A2', 'Item A2'), ('A3', 'Item A3'), ),
                'B': (('B1', 'Item B1'), ('B2', 'Item B2'), ('B3', 'Item B3'), ),
                'C': (('C1', 'Item C1'), ('C2', 'Item C2'), ('C3', 'Item C3'), ),
            }
        }

        # Tree Widget
        self.tree_widget = QtWidgets.QTreeWidget(self)
        
        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_widget)
        self.setLayout(layout)

        # Fill Widget
        self.tree_widget.setColumnCount(len(self.data['headers']))
        self.tree_widget.setHeaderLabels(self.data['headers'])
        root = QtWidgets.QTreeWidgetItem(self.tree_widget)
        for row in sorted(self.data['tree']):
            item = QtWidgets.QTreeWidgetItem(root, [row,])
            item.setFont(0, QtGui.QFont("Times", 10, QtGui.QFont.Bold))

            for subitem_text, subitem_description in self.data['tree'][row]:
                subitem = QtWidgets.QTreeWidgetItem(item, [subitem_text, subitem_description])
                subitem.setForeground(0, QtGui.QColor('red'))
                subitem.setFont(0, QtGui.QFont("Times", 10, QtGui.QFont.Bold))
                subitem.setIcon(1, QtGui.QIcon(icon_boat))
                subitem.setTextAlignment(1, QtCore.Qt.AlignRight)
                subitem.setBackground(1, QtGui.QColor('yellow'))

            self.tree_widget.expandItem(item)
        self.tree_widget.expandItem(root)

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()