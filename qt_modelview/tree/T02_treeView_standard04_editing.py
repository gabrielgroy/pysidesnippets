'''
Simple Tree View example
    It shows how to create a simple list view using QTreeView and QStandardItemModel

    Notice that with QStandardItemModel, we cannot rename the headers
'''
import sys
from Qt import QtCore, QtWidgets, QtGui

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(400, 500)

        # Tree data
        self.data = {
            'headers': ['Item', 'Description'],
            'tree': {
                'A': {('A1', 'Item A1'), ('A2', 'Item A2'), ('A3', 'Item A3'), },
                'B': {('B1', 'Item B1'), ('B2', 'Item B2'), ('B3', 'Item B3'), },
                'C': {('C1', 'Item C1'), ('C2', 'Item C2'), ('C3', 'Item C3'), },
            }
        }

        # Create table, using the Model/View framework
        self.tree_view = QtWidgets.QTreeView()
        self.tree_model = QtGui.QStandardItemModel()
        self.tree_view.setModel(self.tree_model)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_view)
        self.setLayout(layout)

        # Fill View

        # Header
        for i,text in enumerate(self.data['headers']):
            item = QtGui.QStandardItem(text)
            self.tree_model.setHorizontalHeaderItem(i, item)

        # Tree
        rootNode = self.tree_model.invisibleRootItem()
        rootNode.setColumnCount(2)
        for row in sorted(self.data['tree']):
            item = QtGui.QStandardItem(row)
            item.setRowCount(len(self.data['tree'][row]))
            for i, item_data in enumerate(self.data['tree'][row]):
                subitem_name = QtGui.QStandardItem(item_data[0])
                subitem_name.setFlags(\
                    QtCore.Qt.ItemIsEnabled\
                    | QtCore.Qt.ItemIsEditable\
                    # | QtCore.Qt.ItemIsSelectable\
                    # | QtCore.Qt.ItemIsDragEnabled\
                    # | QtCore.Qt.ItemIsDropEnabled\
                    # | QtCore.Qt.ItemIsUserCheckable\
                    )
                item.setChild(i, 0, subitem_name)
                subitem_description = QtGui.QStandardItem(item_data[1])
                subitem_description.setFlags(\
                    QtCore.Qt.ItemIsEnabled\
                    | QtCore.Qt.ItemIsEditable\
                    )
                item.setChild(i, 1, subitem_description)
            rootNode.appendRow(item)
        # Let's expand the tree
        self.tree_view.expandAll()
        self.tree_view.setColumnWidth(0, 80)


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()