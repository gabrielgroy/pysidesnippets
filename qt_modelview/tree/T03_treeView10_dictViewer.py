'''
Tree View to display the contents of a dictionary
'''
import sys
from Qt import QtCore, QtWidgets


TMP_DATA = {"ava": {
    "asset_usd": "S:/pko/chars/A/ava/pko_chdesc_ava.stable8.usda", 
    "groom_data": {
      "original": {
        "curve_prims": {
          "groom_body_barbs": {
            "curveVertexCounts": 581968, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_body/groom_body_barbs", 
            "points": 6372405
          }, 
          "groom_covertWings_barbs": {
            "curveVertexCounts": 152843, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_covertWings/groom_covertWings_barbs", 
            "points": 1776741
          }, 
          "groom_crest_barbs": {
            "curveVertexCounts": 6859, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_crest/groom_crest_barbs", 
            "points": 85927
          }, 
          "groom_head_barbs": {
            "curveVertexCounts": 509953, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_head/groom_head_barbs", 
            "points": 4890471
          }, 
          "groom_mainWings_barbs": {
            "curveVertexCounts": 82130, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_mainWings/groom_mainWings_barbs", 
            "points": 1024039
          }, 
          "groom_mainWings_barbules": {
            "curveVertexCounts": 27349, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_mainWings/groom_mainWings_barbules", 
            "points": 345982
          }, 
          "groom_scapular_barbs": {
            "curveVertexCounts": 7056, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_scapular/groom_scapular_barbs", 
            "points": 77324
          }, 
          "groom_tail_barbs": {
            "curveVertexCounts": 11693, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_tail/groom_tail_barbs", 
            "points": 185823
          }, 
          "groom_tail_barbules": {
            "curveVertexCounts": 23394, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_tail/groom_tail_barbules", 
            "points": 371750
          }, 
          "groom_undercoat": {
            "curveVertexCounts": 626908, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_undercoat/groom_undercoat", 
            "points": 6895988
          }, 
          "guides_body": {
            "curveVertexCounts": 1134, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/guides_body/guides_body", 
            "points": 10345
          }, 
          "guides_covertWings": {
            "curveVertexCounts": 854, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/guides_covertWings/guides_covertWings", 
            "points": 5420
          }, 
          "guides_crest": {
            "curveVertexCounts": 42, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/guides_crest/guides_crest", 
            "points": 284
          }, 
          "guides_head": {
            "curveVertexCounts": 2859, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/guides_head/guides_head", 
            "points": 14981
          }, 
          "guides_mainWings": {
            "curveVertexCounts": 162, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/guides_mainWings/guides_mainWings", 
            "points": 1476
          }, 
          "guides_scapular": {
            "curveVertexCounts": 20, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/guides_scapular/guides_scapular", 
            "points": 304
          }, 
          "guides_tail": {
            "curveVertexCounts": 24, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/guides_tail/guides_tail", 
            "points": 406
          }, 
          "guides_undercoat": {
            "curveVertexCounts": 2039, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/guides_undercoat/guides_undercoat", 
            "points": 14273
          }
        }, 
        "path": "S:/pko/chars/A/ava/opinion/pko_chdeptvar_groom_ava_original.v0116.usda"
      }, 
      "test": {
        "curve_prims": {
          "groom_barbs": {
            "curveVertexCounts": 92634, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_tempWings/groom_lwing/groom_barbs", 
            "points": 1030809
          }, 
          "groom_barbules": {
            "curveVertexCounts": 15876, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_tempWings/groom_lwing/groom_barbules", 
            "points": 200008
          }, 
          "groom_tempBody_1": {
            "curveVertexCounts": 108510, 
            "path": "/ava/layout_c_uctr/anim_c_uctr/aux_c_uctr/world_c_grp/geo_c_grp/hair_c_grp/groom_tempBody/groom_tempBody_1", 
            "points": 1230817
          }
        }, 
        "path": "S:/pko/chars/A/ava/opinion/pko_chdeptvar_groom_ava_test.v0001.usda"
      }
    }, 
    "groom_opinion": "S:/pko/chars/A/ava/opinion/pko_chdeptop_ava_groom.v0114.usda"
  }
}

class DictWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(1200, 800)

        # Create table, using the Model/View framework
        self.tree_view = QtWidgets.QTreeView()
        
        # Connect the view with the model
        self.tree_model = DictModel(TMP_DATA, self)
        self.tree_view.setModel(self.tree_model)
        self.tree_view.setColumnWidth(0, 300)
        self.tree_view.expandAll()

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_view)
        self.setLayout(layout)

class DictModel(QtCore.QAbstractItemModel):
    '''
    '''
    def __init__(self, data=None, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        self.headers = []
        self._data = {}
        self.rootItem = DictItem()
        if data:
            self.headers = ('Key', 'Value')
            self._data = data
            self.fillModel(self._data, self.rootItem)

    def fillModel(self, d, parent_item):
        for k in sorted(list(d)):
            if isinstance(d[k], dict):
                item = DictItem({'key': k, 'value':''}, parent_item)
                parent_item.appendChild(item)
                self.fillModel(d[k], item)
            else:
                item = DictItem({'key': k, 'value':d[k]}, parent_item)
                parent_item.appendChild(item)

    # Data query methods. With these, the view asks for data to the model
    # passing an index
    def rowCount(self, parent=QtCore.QModelIndex()):
        '''
        The view will ask the model about the number of rows of a index calling
        this method, passing the item index as the parent argument
        '''
        if parent.column() > 0:
            return 0
        # We need to identify the item referenced by the passed index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        return parentItem.childCount()

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.headers)

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.headers[section]
        
    def data(self, index, role):
        ''' Give me the data of the item of this index in these coordinates
            The view will query the model about the data of an item for a given role
            passing an index that contains the coordinates
            It passes an index, because the view doesn't have direct access to the items
            It is the model who identifies the item that corresponds to the index,
            and gets the piece of data that corresponds to the role
            We can think about the role as the key of a dictionary stored by the item
        '''
        # As always, if the index is not valid, it's because it refers to the root item
        if not index.isValid():
            return None

        # As we may not be using all the possible roles, we filter to consider only the ones
        # we care about
        elif not role in (QtCore.Qt.DisplayRole, QtCore.Qt.UserRole): # QtCore.Qt.DecorationRole, QtCore.Qt.ForegroundRole):
            return None

        # Once we pass the filter, we get the item, ask for the data of the role and return it
        item = index.internalPointer()
        # if role == QtCore.Qt.DisplayRole:
        return item.data(index.column())

    # Index building methods. With these, the view asks the model to build indexes
    # based on row, column, and parent
    def rootIndex(self):
        '''
            Metodo necesario, porque lo pide el view
        '''
        return self.createIndex(0, 0, self.rootItem)

    def index(self, row, column, parent=QtCore.QModelIndex()):
        ''' Create an index with the item of the coordinates row and column under parent

            With this method, the view asks the model for the data for a cell
            First it asks the model to build an index for a row and column, under another
            index identified as 'parent'. Then later, this index will be what the view will
            pass to the model to ask for the data of the index/item
        '''
        # The first step is to check if the passed args are valid
        # The model should tell us if an index is possible or not for the row, col and parent
        if not self.hasIndex(row, column, parent):
            # If it is not possible , the model returns an empty invalid index
            return QtCore.QModelIndex()

        # To build a valid index, we need an item
        # Let's identify the item referenced by the parent index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
            
        # Once we have the parent index, we get the child item
        # For this particular example we only need the row
        childItem = parentItem.child(row)
        if childItem:
            # If the item exists, we can create and return the index
            index = self.createIndex(row, column, childItem)
            return index
        else:
            # If not, we return and empty invalud index
            return QtCore.QModelIndex()

    def parent(self, index):
        '''
            In this method, starting from the passed index, we have to identify the item
            and build an index for its parent
        '''
        if not index.isValid():
            return QtCore.QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QtCore.QModelIndex()

        return self.createIndex(childItem.row(), 0, parentItem)

    # Custom extra methods
    def addItem(self, item_data, parent_index=None):
        ''' Added funtion to edit the model from outside '''
        if (parent_index is None) or (parent_index=='') or (not parent_index.isValid()):
            parent_index = self.rootIndex()
            parent_item = self.rootItem
        else:
            parent_item = parent_index.internalPointer()
        item = DictItem(item_data, parent_item)
        # MUTEX.lock()
        self.beginInsertRows(parent_index, parent_item.childCount(), parent_item.childCount()+1)
        parent_item.appendChild(item)
        self.endInsertRows()
        # MUTEX.unlock()
        print("Added item: {}".format(item_data))
        self.layoutChanged.emit()
        
class DictItem(object):
    '''
    We create this custom class for the items because we need an object that
    keeps track of it's children and parent, allowing a tree structure
    '''
    def __init__(self, item_data=None, parent=None):
        '''
        At the initialization we store the received data and
        a reference to the parent
        - item_data: dictionary with keys:
            - key
            - value
        - parent: (DictItem) parent item for this item
        '''
        self.parentItem = parent
        if item_data is None:
            item_data = {}
        self.itemData = item_data
        self.childItems = []

    def appendChild(self, item):
        ''' Method to add children to this item '''
        self.childItems.append(item)

    def child(self, row):
        ''' Method to access a child '''
        return self.childItems[row]

    def childCount(self):
        ''' Returns the children amount '''
        return len(self.childItems)

    def columnCount(self):
        ''' Returns the columns amount
        In this case it is always two: key and value '''
        return 2

    def data(self, column):
        '''
        Method to access the item's data.
        The view will query the model, and the model will query the item
        Args:
        - column
            0 is the key, 1 the value
        '''
        return self.itemData.get({0:'key', 1:'value'}.get(column, ''), '')

    def setData(self, column, value):
        self.itemData[{0:'key', 1:'value'}.get(column)] = value

    def parent(self):
        ''' Returns a reference to the parent '''
        return self.parentItem

    def row(self):
        '''
        Returns the row number in which this item is placed
        under the children of the parent item
        '''
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = DictWindow()
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()