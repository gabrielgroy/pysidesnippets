'''
Simple Tree View example - Editing
    To enable editing of the items, we need to reimplement the methods 'flags' and 'setData'
    The returned flags need to include QtCore.Qt.ItemIsEditable
    'setData' is in charge of storing the edited data in the model
    'setData' will get the item from the index and call the item's own 'setData' function
    to actually store the the data in the item.
'''
import sys
from Qt import QtCore, QtWidgets

DEFAULT_VALUE = 'A'

class ComboBoxDelegate(QtWidgets.QStyledItemDelegate):
    def __init__(self, *args, **kwargs):
        super(ComboBoxDelegate, self).__init__(*args, **kwargs)

    def createEditor(self, parent, option, index):
        if index.column()!=1:
            return super(ComboBoxDelegate, self).createEditor(parent, option, index)
        editor = QtWidgets.QComboBox(parent)
        editor.addItems(['A', 'B', 'C'])
        editor.currentIndexChanged.connect(self.emitCommitData)
        return editor

    def setEditorData(self, editor, index):
        if index.column()!=1:
            return super(ComboBoxDelegate, self).setEditorData(editor,index)
        value = index.model().data(index, QtCore.Qt.EditRole)
        pos = editor.findText(value, QtCore.Qt.MatchExactly)
        editor.setCurrentIndex(pos)

    def setModelData(self, editor, model, index):
        if index.column()!=1:
            return super(ComboBoxDelegate, self).setModelData(editor, model, index)
        value = editor.currentText()
        model.setData(index, value, QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        '''
        It is the responsibility of the delegate to manage the editor's geometry.
        The geometry must be set when the editor is created, and when the item's size or position
        in the view is changed. Fortunately, the view provides all the necessary geometry information
        inside a view 'option' object.
        '''
        editor.setGeometry(option.rect)
        editor.showPopup() # <-- To expand the combo list without requiring a second click

    def emitCommitData(self):
        self.commitData.emit(self.sender())

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(400, 500)

        # Tree data
        self.data = {
            'headers': ['Item', 'Description'],
            'tree': {
                'A': {('A1', DEFAULT_VALUE), ('A2', DEFAULT_VALUE), ('A3', DEFAULT_VALUE), },
                'B': {('B1', DEFAULT_VALUE), ('B2', DEFAULT_VALUE), ('B3', DEFAULT_VALUE), },
                'C': {('C1', DEFAULT_VALUE), ('C2', DEFAULT_VALUE), ('C3', DEFAULT_VALUE), },
            }
        }

        # Create table, using the Model/View framework
        self.tree_view = QtWidgets.QTreeView()
        
        # Connect the view with the model
        self.tree_model = MyTreeModel(self.data, self)
        self.tree_view.setModel(self.tree_model)
        self.tree_view.setItemDelegate(ComboBoxDelegate())
        self.tree_view.expandAll()

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_view)
        self.setLayout(layout)

class MyTreeModel(QtCore.QAbstractItemModel):
    '''
        Our model is a subclass of QtCore.QAbstractItemModel
        We cannot use abstract classes directly. We always need to subclass them
        and reimplement some methods
        
        In the same way, for our items, we usually will need to define a custom class.
        
        The model needs to work with the view or views, because we can have multiple views
        for the same model.
        The model stores the data and communicates with the view through indexes.
        Indexes and items in the model are two completely different things, but they're related.
        Indexes are temporary objects used for the communication and expire aftwerwards.
        
        It is responsability of the model to build the indexes and relate them with the items.
        
        All of these methods need to be reimplemented.
        
        The view doesn't create the indexes on its own. It asks the model to create them.
        The three methods in the model that build indexes are:
        - rootIndex, to build the index for the root item
        - index, to build indexes for items that live inside other items
        - parent, to build the index that is the parent of another index
        
        The methods with which the view asks the model about the data are:
        - rowCount
        - columnCount
        - headerData
        - data
    '''
    def __init__(self, data, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        self.headers = data['headers']
        self._data = data['tree']
        self.rootItem = MyItem()

        # Fill model with data
        for row in sorted(self._data):
            item = MyItem({'name': row, 'description':''}, parent=self.rootItem)
            self.rootItem.appendChild(item)
            for subitem_text, subitem_description in self._data[row]:
                subitem = MyItem({'name':subitem_text, 'description':subitem_description}, parent=item)
                item.appendChild(subitem)

    # Data query methods. With these, the view asks for data to the model
    # passing an index
    def rowCount(self, parent=QtCore.QModelIndex()):
        '''
        The view will ask the model about the number of rows of a index calling
        this method, passing the item index as the parent argument
        '''
        if parent.column() > 0:
            return 0
        # We need to identify the item referenced by the passed index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        return parentItem.childCount()

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.headers)

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.headers[section]
        
    # When we double click an item to edit, the flags function is called to check
    # if the item has the ItemIsEditable flag or not. If not, we won't be able to edit
    def flags(self, index):
        if not index.isValid():
            return None
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable

    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        ''' Give me the data of the item of this index in these coordinates
            The view will query the model about the data of an item for a given role
            passing an index that contains the coordinates
            It passes an index, because the view doesn't have direct access to the items
            It is the model who identifies the item that corresponds to the index,
            and gets the piece of data that corresponds to the role
            We can think about the role as the key of a dictionary stored by the item
        '''
        # As always, if the index is not valid, it's because it refers to the root item
        if not index.isValid():
            return None

        # As we may not be using all the possible roles, we filter to consider only the ones
        # we care about
        elif not role in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole): # QtCore.Qt.DecorationRole, QtCore.Qt.UserRole, QtCore.Qt.ForegroundRole):
            return None

        # Once we pass the filter, we get the item, ask for the data of the role and return it
        item = index.internalPointer()
        if role in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):
            return item.data(index.column())

    # To actually edit the data, this method needs to be reimplemented
    def setData(self, index, value, role):
        # The index needs to be valid
        if not index.isValid():
            return False
        # and the role has to be EditRole
        if role != QtCore.Qt.EditRole:
            return False
        # From the index we get the info we need to edit the underlying data
        row = index.row()
        col = index.column()
        item = index.internalPointer()
        item.setData(col, value)
        # After modifying the data we have to explicitly emit the signal dataChanged
        # The first arg is the topleft index and the second arg the bottomright index
        # In this case, they're the same
        self.dataChanged.emit(index, index)
        return True

    # Index building methods. With these, the view asks the model to build indexes
    # based on row, column, and parent
    def rootIndex(self):
        ''' Metodo necesario, porque lo pide el view '''
        return self.createIndex(0, 0, self.rootItem)

    def index(self, row, column, parent=QtCore.QModelIndex()):
        ''' Create an index with the item of the coordinates row and column under parent

            With this method, the view asks the model for the data for a cell
            First it asks the model to build an index for a row and column, under another
            index identified as 'parent'. Then later, this index will be what the view will
            pass to the model to ask for the data of the index/item
        '''
        # The first stel is to check if the passed args are valid
        # The model should tell us if an index is possible or not for the row, col and parent
        if not self.hasIndex(row, column, parent):
            # If it is not possible , the model returns an empty invalid index
            return QtCore.QModelIndex()
        
        # To build a valid index, we need an item
        # Let's identify the item referenced by the parent index
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
            
        # Once we have the parent index, we get the child item
        # For this particular example we only need the row
        childItem = parentItem.child(row)
        if childItem:
            # If the item exists, we can create and return the index
            index = self.createIndex(row, column, childItem)
            return index
        else:
            # If not, we return and empty invalud index
            return QtCore.QModelIndex()

    def parent(self, index):
        '''
            In this method, starting from the passed index, we have to identify the item
            and build an index for its parent
        '''
        if not index.isValid():
            return QtCore.QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QtCore.QModelIndex()

        return self.createIndex(childItem.row(), 0, parentItem)

class MyItem(object):
    '''
    We create this custom class for the items because we need an object that
    keeps track of it's children and parent, allowing a tree structure
    '''
    def __init__(self, item_data=None, parent=None):
        '''
        At the initialization we store the received data and
        a reference to the parent
        - item_data: dictionary with keys:
            - name
            - description
        - parent: (MyItem) parent item for this item
        '''
        self.parentItem = parent
        if item_data is None:
            item_data = {}
        self.itemData = item_data
        self.childItems = []

    def appendChild(self, item):
        ''' Method to add children to this item '''
        self.childItems.append(item)

    def child(self, row):
        ''' Mathod to access a child '''
        return self.childItems[row]

    def childCount(self):
        ''' Returns the children amount '''
        return len(self.childItems)

    def columnCount(self):
        ''' Returns the columns amount
        In this examples, it is always two: name and description '''
        return 2

    def data(self, column):
        '''
        Method to access the item's data.
        The view will query the model, and the model will wuery the item
        Args:
        - column
            0 is the name, 1 the description
        '''
        return self.itemData.get({0:'name', 1:'description'}.get(column, ''), '')

    def setData(self, column, value):
        '''
        Method to set data into an item
        '''
        if column > len(self.itemData):
            return False
        self.itemData[{0:'name', 1:'description'}[column]] = value

    def parent(self):
        ''' Returns a reference to the parent '''
        return self.parentItem

    def row(self):
        '''
        Returns the row number in which this item is placed
        under the children of the parent item
        '''
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()