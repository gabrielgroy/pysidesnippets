'''
Simple Tree Widget example
    The size of the icons is defined at the tree widget level with:
        tree_widget.setIconSize
    Icons are positioned at the left
    If we want to only display a centered icon, the easiest approach is
    to add a label widget to the cell with
        tree_widget.setItemWidget
    Note that we don't have this method in QTableWidget or QListWidget, where
    we have to use setIndexWidget instead 
'''
import os
import sys
from Qt import QtWidgets, QtGui, QtCore

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")

ICON_SIZE = 100

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 
        self.resize(500, 700)

        # Tree data
        self.data = {
            'headers': ['Item', 'Standard Icon', 'Widget Icon'],
            'tree': {
                'A': (('A1', 'Item A1', ''), ('A2', 'Item A2', ''), ('A3', 'Item A3', ''), ),
                'B': (('B1', 'Item B1', ''), ('B2', 'Item B2', ''), ('B3', 'Item B3', ''), ),
                'C': (('C1', 'Item C1', ''), ('C2', 'Item C2', ''), ('C3', 'Item C3', ''), ),
            }
        }

        # Tree Widget
        self.tree_widget = QtWidgets.QTreeWidget(self)
        self.tree_widget.setIconSize(QtCore.QSize(ICON_SIZE,ICON_SIZE))
        
        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tree_widget)
        self.setLayout(layout)

        # Fill Widget
        self.tree_widget.setColumnCount(len(self.data['headers']))
        self.tree_widget.setHeaderLabels(self.data['headers'])
        root = QtWidgets.QTreeWidgetItem(self.tree_widget)
        for row in sorted(self.data['tree']):
            item = QtWidgets.QTreeWidgetItem(root, [row,])

            for subitem_text, subitem_t1, subitem_t2 in self.data['tree'][row]:
                subitem = QtWidgets.QTreeWidgetItem(item, [subitem_text, subitem_t1, subitem_t2])

                # Standard icon
                subitem.setIcon(1, QtGui.QIcon(icon_boat))
                subitem.setBackground(1, QtGui.QColor('lightgray'))
                self.tree_widget.setColumnWidth(1, 180)

                # Custom widget icon
                subitem.setBackground(2, QtGui.QColor('gray'))
                icon_pixmap = QtGui.QPixmap(icon_boat)#.scaledToHeight(ICON_SIZE)
                label = QtWidgets.QLabel(self)
                label.setPixmap(icon_pixmap)
                label.setAlignment(QtCore.Qt.AlignCenter)
                label.setContentsMargins(0,0,0,0)
                label.setAutoFillBackground(False)
                self.tree_widget.setItemWidget(subitem, 2, label)

            self.tree_widget.expandItem(item)
        self.tree_widget.expandItem(root)

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()