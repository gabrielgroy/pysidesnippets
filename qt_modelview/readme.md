# Model / View examples

## List views

Some examples on how to create lists views:

- `L01_listWidget01.py` Simple example with QListWidget
- `L01_listWidget02_formatting.py` Simple example with QListWidget, customizing colors and fonts
- `L01_listWidget03_icons.py` Simple example with QListWidget, implementing icons in two ways
- `L01_listWidget04_fill_wo_threads.py` QListWidget being filled after clicking on a button (no threads involved)
- `L01_listWidget05_fill_w_threads.py` QListWidget being filled from a parallel thread
- `L01_listWidget06_fill_w_threads2.py` QListWidget being filled from a parallel thread, adding DEBUG option
- `L01_listWidget07_fill_w_threads3.py` QListWidget being filled from a parallel thread, alternative way
- `L01_listWidget08_editing.py` Editable QListWidget

- `L02_listView_standard_01.py` Simple list example with QListView and QStandardItemModel
- `L02_listView_standard_02_formatting.py` QListView and QStandardItemModel, implementing some formatting
- `L02_listView_standard_03_icons.py` QListView and QStandardItemModel, implementing icons
- `L02_listView_standard_0_editing_.py` QListView and QStandardItemModel, editable

- `L03_listView01_custom.py` Simple example with QListView and QAbstractListModel
- `L03_listView02_formatting.py` Formatting example using QListView and QAbstractListModel
- `L03_listView02_formatting_with_delegate.py` Formatting example using a QStyledItemDelegate with QListView and QAbstractListModel
- `L03_listView03_icons.py` Simple example with QListView and QAbstractListModel, implementing icons in two ways
- `L03_listView04_and_threads.py` QListView and QAbstractListModel being filled from a parallel thread
- `L03_listView05_editing.py` Editing list example with QListView and QAbstractListModel
- `L03_listView06_delegateSpinBox.py` QListView and QAbstractListModel implementing a simple SpinBox delegate
- `L03_listView06_delegateComboBox.py` QListView and QAbstractListModel implementing a simple ComboBox delegate
- `L03_listView06_delegateHtml.py` QListView and QAbstractListModel implementing a html delegate

## Table views

Some examples on how to create table views:

- `T01_tableWidget01.py` Simple example with QTableWidget
- `T01_tableWidget02_formatting.py` Simple example with QTableWidget, customizing colors and fonts
- `T01_tableWidget03_icons.py` Simple example with QTableWidget, implementing icons in two ways
- `T01tableWidget04_fill_wo_threads.py` QTableWidget being filled after clicking on a button (no threads involved)
- `T01tableWidget05_fill_w_threads.py` QTableWidget being filled from a parallel thread

- `T02_tableView_standard01.py` Simple example with QTableView and QStandardItemModel
- `T02_tableView_standard02_formatting.py` QTableView and QStandardItemModel, customizing colors and fonts
- `T02_tableView_standard03_icons.py` QTableView and QStandardItemModel, implementing icons
- `T02_tableView_standard04_editing.py` QTableView and QStandardItemModel, editable

- `T03_tableView01_custom.py` Simple example with QTableView and QAbstractTableModel
- `T03_tableView02_formatting.py` QTableView and QAbstractTableModel, customizing colors and fonts
- `T03_tableView03_icons.py` QTableView and QAbstractTableModel, implementing icons
- `T03_tableView04_and_threads.py` QTableView and QAbstractTableModel being filled from a parallel thread
- `T03_tableView05_editing.py` QTableView and QAbstractTableModel, editable
- `T03_tableView06_delegate_spinBox.py` QTableView and QAbstractTableModel, implementing a simple SpinBox delegate
- `T03_tableView07_delegate_comboBox.py` QTableView and QAbstractTableModel, implementing a simple ComboBox delegate

## Tree views

Some examples on how to create tree views:

- `T01_treeWidget01.py` Simple example with QTreeWidget
- `T01_treeWidget02_formatting.py` Simple example with QTreeWidget, customizing colors and fonts
- `T01_treeWidget03_icons.py` Simple example with QTreeWidget, implementing icons in two ways
- `T01_treeWidget04_fill_wo_threads.py` QTreeWidget being filled after clicking on a button (no threads involved)
- `T01_treeWidget05_editing.py` Simple example with QTreeWidget, editable

- `T02_treeView_standard01.py` Simple example with QTreeView and QStandardItemModel
- `T02_treeView_standard02_formatting.py` QTreeView and QStandardItemModel, customizing colors and fonts
- `T02_treeView_standard03_icons.py` QTreeView and QStandardItemModel, implementing icons
- `T02_treeView_standard04_editing.py` QTreeView and QStandardItemModel, editable

- `T03_treeView01_custom.py` Simple example with QTreeView and QAbstractItemModel
- `T03_treeView02_lazyexp_approach1.py` QTreeView and QAbstractItemModel example with lazy population, approach1
- `T03_treeView02_lazyexp_approach2.py` QTreeView and QAbstractItemModel example with lazy population, approach2
- `T03_treeView03_formatting.py` QTreeView and QAbstractItemModel, customizing colors and fonts
- `T03_treeView04_icons.py` QTreeView and QAbstractItemModel, implementing icons
- `T03_treeView05_threads.py` QTreeView and QAbstractItemModel, being filled from a parallel thread
- `T03_treeView06_editing.py` QTreeView and QAbstractItemModel, editable
- `T03_treeView07_delegate_spinBox.py` QTreeView and QAbstractItemModel, implementing a simple SpinBox delegate
- `T03_treeView08_delegate_comboBox.py` QTreeView and QAbstractItemModel, implementing a simple ComboBox delegate
