'''
    To add a context menu to any subclass of QAbstractItemview, as is the case for
    QListView, QTableView and QTreeView, we need to subclass and reimplement the
    'contextMenuEvent'

'''
from Qt import QtCore, QtGui, QtWidgets
from functools import partial

class MyView(QtWidgets.QTableView):
    def __init__(self, parent=None):
        QtWidgets.QTableView.__init__(self, parent=None)
        
        # Internal list of actions
        self.actions = [
            { 'label': 'Action1', 'callback': self.func1 },
            { 'label': 'Action2', 'callback': self.func2 },
        ]

    def contextMenuEvent(self, event):
        menu = self.buildMenu()
        if menu:
            action = menu.exec_(self.mapToGlobal(event.pos()+QtCore.QPoint(100,0)))

    def buildMenu(self):
        menu = QtWidgets.QMenu()

        # Get the selection if the actions to display depend on selection
        selected_indexes = self.selectionModel().selectedIndexes()
        currentIndex = self.selectionModel().currentIndex()

        # Build the actions
        for item in self.actions:
            action = menu.addAction(item['label'])
            action.setObjectName(item['label'].replace(' ', ''))
            callback = item['callback']
            # or
            callback = partial(item['callback'], selected_indexes)
            action.triggered.connect(callback)
        return menu

    def func1(self, sel_indexes):
        pass

    def func2(self, sel_indexes):
        pass
