'''
Simple List Widget example
    It shows how to create a simple list view using QListWidget

    The items, QtWidgets.QListWidgetItem, can be formatted with the following methods:
    - setText()
    - setIcon()
    - setFont()
    - setForeground()
    - setBackground()
    - setTextAlignment
    - setToolTip

    By default, items are enabled, selectable, checkable, and can be the source of drag and drop operations.
    Each item's flags can be changed by calling setFlags() with the appropriate value (see Qt.ItemFlags).
    Checkable items can be checked, unchecked and partially checked with the setCheckState() function.
    The corresponding checkState() function indicates the item's current check state.

    The isHidden() function can be used to determine whether the item is hidden.
    To hide an item, use setHidden().
'''
import os
import sys
from Qt import QtCore, QtGui, QtWidgets

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")
icon_car = os.path.join(basedir, "../../resources/car.png")
icon_house = os.path.join(basedir, "../../resources/house.png")

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        listWidget = QtWidgets.QListWidget(self)
        # Icon size is defined in the list widget
        listWidget.setIconSize(QtCore.QSize(100,100))

        item = QtWidgets.QListWidgetItem()
        item.setText('Red')
        item.setIcon(QtGui.QIcon(icon_boat))
        item.setForeground(QtGui.QColor('red')) # or QtGui.QColor(QtCore.Qt.red)
        item.setBackground(QtGui.QColor('yellow'))
        item.setTextAlignment(QtCore.Qt.AlignLeft)
        item.setToolTip("Red color item")
        item.setFont(QtGui.QFont("Times", 20, QtGui.QFont.Normal))
        listWidget.addItem(item)
        
        item = QtWidgets.QListWidgetItem()
        item.setText('Green')
        item.setIcon(QtGui.QIcon(icon_car))
        item.setForeground(QtGui.QColor('green')) # or QtGui.QColor(QtCore.Qt.green)
        item.setBackground(QtGui.QColor('darkGray'))
        item.setTextAlignment(QtCore.Qt.AlignRight)
        item.setToolTip("Green color item")
        item.setFont(QtGui.QFont("Times", 15, QtGui.QFont.Bold, QtGui.QFont.StyleItalic))
        listWidget.addItem(item)

        item = QtWidgets.QListWidgetItem()
        item.setText('Blue')
        item.setIcon(QtGui.QIcon(icon_house))
        item.setForeground(QtGui.QColor('blue')) # or QtGui.QColor(QtCore.Qt.blue)
        item.setBackground(QtGui.QColor('cyan'))
        item.setTextAlignment(QtCore.Qt.AlignHCenter)
        item.setToolTip("Blue color item")
        item.setFont(QtGui.QFont("Times", 10, QtGui.QFont.DemiBold))
        listWidget.addItem(item)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(listWidget)
        self.setLayout(layout)

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()
