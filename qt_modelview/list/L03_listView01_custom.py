'''
Simple List View example
    It shows how to create a simple list view using QListView and QAbstractListModel

    With QAbstractListModel we need to subclass and reimplement some methods.
'''
import sys 
from Qt import QtCore, QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)

        # Create list, using the Model/View framework
        list_data = [1, 2, 3, 4]
        list_model = MyListModel(list_data, self)
        list_view = QtWidgets.QListView()
        
        # Connect the view with the model
        list_view.setModel(list_model)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(list_view)
        self.setLayout(layout)

class MyListModel(QtCore.QAbstractListModel):
    def __init__(self, datain, parent=None, *args):
        """ datain: a list where each item will be a row
        """
        QtCore.QAbstractListModel.__init__(self, parent, *args)
        self.listdata = datain

    # When we subclass QAbstractListModel, we need to reimplement at lease: 'rowCount' and 'data'
    
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.listdata)

    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        if index.isValid() and role == QtCore.Qt.DisplayRole:
            return self.listdata[index.row()]
        else:
            return None

def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

