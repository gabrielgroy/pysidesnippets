'''
Simple List View example
    It shows how to create a simple list view using QListView and QAbstractListModel

    With QAbstractListModel, the formatting is established by the model in the 'data' function
'''
import os
import sys 
from Qt import QtCore, QtGui, QtWidgets

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")
icon_car = os.path.join(basedir, "../../resources/car.png")
icon_house = os.path.join(basedir, "../../resources/house.png")

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)

        # Create list, using the Model/View framework
        list_model = MyListModel(self)
        list_view = QtWidgets.QListView()
        # Icon size is defined in the list view widget
        list_view.setIconSize(QtCore.QSize(100,100))

        # The list can be changed into Icon Mode:
        # list_view.setViewMode(QtWidgets.QListView.IconMode)
        # list_view.setResizeMode(QtWidgets.QListView.Adjust)
        
        # Connect the view with the model
        list_view.setModel(list_model)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(list_view)
        self.setLayout(layout)

class MyListModel(QtCore.QAbstractListModel):
    def __init__(self, parent=None, *args):
        """ datain: a list where each item will be a row
        """
        QtCore.QAbstractListModel.__init__(self, parent, *args)

    # When we subclass QAbstractListModel, we need to reimplement at lease: 'rowCount' and 'data'
    
    def rowCount(self, parent=QtCore.QModelIndex()):
        return 3

    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        if not index.isValid():
            return
        if index.row() == 0:
            if role == QtCore.Qt.DisplayRole:
                return 'Red'
            elif role == QtCore.Qt.DecorationRole:
                return QtGui.QIcon(icon_boat)
            elif role == QtCore.Qt.ForegroundRole:
                return QtGui.QColor('red') # or QtGui.QColor(QtCore.Qt.red)
            elif role == QtCore.Qt.BackgroundRole:
                return QtGui.QColor('yellow')
            elif role == QtCore.Qt.TextAlignmentRole:
                return QtCore.Qt.AlignLeft
            elif role == QtCore.Qt.ToolTipRole:
                return "Red color item"
            elif role == QtCore.Qt.FontRole:
                return QtGui.QFont("Times", 20, QtGui.QFont.Normal)
        if index.row() == 1:
            if role == QtCore.Qt.DisplayRole:
                return 'Green'
            elif role == QtCore.Qt.DecorationRole:
                return QtGui.QIcon(icon_car)
            elif role == QtCore.Qt.ForegroundRole:
                return QtGui.QColor('green') # or QtGui.QColor(QtCore.Qt.red)
            elif role == QtCore.Qt.BackgroundRole:
                return QtGui.QColor('darkGray')
            elif role == QtCore.Qt.TextAlignmentRole:
                return QtCore.Qt.AlignRight
            elif role == QtCore.Qt.ToolTipRole:
                return "Green color item"
            elif role == QtCore.Qt.FontRole:
                return QtGui.QFont("Times", 15, QtGui.QFont.Bold, QtGui.QFont.StyleItalic)
        if index.row() == 2:
            if role == QtCore.Qt.DisplayRole:
                return 'Blue'
            elif role == QtCore.Qt.DecorationRole:
                return QtGui.QIcon(icon_house)
            elif role == QtCore.Qt.ForegroundRole:
                return QtGui.QColor('blue') # or QtGui.QColor(QtCore.Qt.red)
            elif role == QtCore.Qt.BackgroundRole:
                return QtGui.QColor('cyan')
            elif role == QtCore.Qt.TextAlignmentRole:
                return QtCore.Qt.AlignHCenter
            elif role == QtCore.Qt.ToolTipRole:
                return "Blue color item"
            elif role == QtCore.Qt.FontRole:
                return QtGui.QFont("Times", 10, QtGui.QFont.DemiBold)
        

def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

