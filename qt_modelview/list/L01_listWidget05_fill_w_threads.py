'''
Simple List Widget example with threads
    It shows how to create a simple list view using QListWidget,
    triggering the fill of the list with a button, and filling
    it from another thread using signals

    We're passing the item through the 'addItem' signal to the
    'addItem' slot of the QListWidget
'''
import sys
import time
from Qt import QtCore, QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        # List
        self.listWidget = QtWidgets.QListWidget(self)

        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.listWidget)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        # Worker setup
        self.worker = Worker()
        self.worker.addItem.connect(self.listWidget.addItem)
        self.worker.updateProgress.connect(self.progressBar.setValue)
        self.pushButton.clicked.connect(self.worker.start)

class Worker(QtCore.QThread):
    addItem = QtCore.Signal(object)
    updateProgress = QtCore.Signal(int)
    
    def run(self):
        for i in range(10):
            item = QtWidgets.QListWidgetItem(str(i+1))
            self.addItem.emit(item)
            self.updateProgress.emit(10*(i+1))
            print("Added item {}".format(i+1))
            time.sleep(.25)
    

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()