'''
Simple List Widget - Editing
    To edit the items of a list widget, we simply need to set the appropriate flags
    The thing with the flags though, is that there are some set by default, so to add
    one more, we also need to explicitly set the default ones, ore they get lost

    The default ones are:
    - QtCore.Qt.ItemIsEnabled
    - QtCore.Qt.ItemIsSelectable
    - QtCore.Qt.ItemIsDragEnabled
    - QtCore.Qt.ItemIsDropEnabled
    - QtCore.Qt.ItemIsUserCheckable
'''
import sys
from Qt import QtWidgets, QtCore

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        self.listWidget = QtWidgets.QListWidget(self)

        for item_data in range(8):
            item = QtWidgets.QListWidgetItem(str(item_data))

            # Here is where we set the flags, and we need to add
            # QtCore.Qt.ItemIsEditable to the default ones, or at least
            # QtCore.Qt.ItemIsEnabled
            item.setFlags(\
                QtCore.Qt.ItemIsEnabled\
                | QtCore.Qt.ItemIsEditable\
                # | QtCore.Qt.ItemIsSelectable\
                # | QtCore.Qt.ItemIsDragEnabled\
                # | QtCore.Qt.ItemIsDropEnabled\
                # | QtCore.Qt.ItemIsUserCheckable\
                )

            self.listWidget.addItem(item)

        # Button
        button = QtWidgets.QPushButton(self)
        button.setText("Print Data")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.listWidget)
        layout.addWidget(button)
        self.setLayout(layout)

        button.clicked.connect(self.printData)
    
    def printData(self):
        for i in range(self.listWidget.count()):
            item = self.listWidget.item(i)
            print(item.text())

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()
