'''
Simple List View
'''
import os
import sys 
from Qt import QtCore, QtGui, QtWidgets

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")
icon_car = os.path.join(basedir, "../../resources/car.png")
icon_house = os.path.join(basedir, "../../resources/house.png")

ICON_SIZE = 100

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)

        # Create list, using the Model/View framework
        list_model = QtGui.QStandardItemModel()
        list_view = QtWidgets.QListView()
        # Icon size is defined in the list view widget
        list_view.setIconSize(QtCore.QSize(ICON_SIZE,ICON_SIZE))
        
        # Connect the view with the model
        list_view.setModel(list_model)

        ## Icon in the cell in the standard way
        item = QtGui.QStandardItem()
        item.setIcon(QtGui.QIcon(icon_boat))
        list_model.appendRow(item)
        
        ## Icon in the cell through a widget
        item = QtGui.QStandardItem()
        # As there is no text there is no size hint,
        # so we need to provide one 
        item.setSizeHint(QtCore.QSize(ICON_SIZE,ICON_SIZE))
        list_model.appendRow(item)

        # We get the index from the item
        index = list_model.index(1, 0, QtCore.QModelIndex())
        # We now create the label with the image
        icon_pixmap = QtGui.QPixmap(icon_boat)#.scaledToHeight(ICON_SIZE)
        label = QtWidgets.QLabel(self)
        label.setPixmap(icon_pixmap)
        label.setAlignment(QtCore.Qt.AlignCenter)
        label.setContentsMargins(0,0,0,0)
        label.setAutoFillBackground(False)
        # And finally we add the widget to the item/index
        list_view.setIndexWidget(index, label)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(list_view)
        self.setLayout(layout)



def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

