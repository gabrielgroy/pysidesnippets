'''
Simple List View - editing example
        It shows how to create a simple list view using QListView and QStandardItemModel

        QStandardItemModel requires to create the contained items with QStandardItem and
        add them to the model.
        It's similar to using QListWidget.

        Similiarly to QListWidgetItem, the QStandardItem(s) can be formatted with the same functions:
        - setText()
        - setIcon()
        - setFont()
        - setForeground()
        - setBackground()
        - setTextAlignment
        - setToolTip

        This view does not display horizontal or vertical headers
'''
import os
import sys 
from Qt import QtCore, QtGui, QtWidgets

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")
icon_car = os.path.join(basedir, "../../resources/car.png")
icon_house = os.path.join(basedir, "../../resources/house.png")


class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)

        # Create list, using the Model/View framework
        list_model = QtGui.QStandardItemModel()
        list_view = QtWidgets.QListView()
        # Icon size is defined in the list view widget
        list_view.setIconSize(QtCore.QSize(100,100))
        
        # Connect the view with the model
        list_view.setModel(list_model)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(list_view)
        self.setLayout(layout)

        # Fill data
        item = QtGui.QStandardItem()
        item.setText('Red')
        item.setIcon(QtGui.QIcon(icon_boat))
        item.setForeground(QtGui.QColor('red')) # or QtGui.QColor(QtCore.Qt.red)
        item.setBackground(QtGui.QColor('yellow'))
        item.setTextAlignment(QtCore.Qt.AlignLeft)
        item.setToolTip("Red color item")
        item.setFont(QtGui.QFont("Times", 20, QtGui.QFont.Normal))
        list_model.appendRow(item)
        
        item = QtGui.QStandardItem()
        item.setText('Green')
        item.setIcon(QtGui.QIcon(icon_car))
        item.setForeground(QtGui.QColor('green')) # or QtGui.QColor(QtCore.Qt.green)
        item.setBackground(QtGui.QColor('darkGray'))
        item.setTextAlignment(QtCore.Qt.AlignRight)
        item.setToolTip("Green color item")
        item.setFont(QtGui.QFont("Times", 15, QtGui.QFont.Bold, QtGui.QFont.StyleItalic))
        list_model.appendRow(item)

        item = QtGui.QStandardItem()
        item.setText('Blue')
        item.setIcon(QtGui.QIcon(icon_house))
        item.setForeground(QtGui.QColor('blue')) # or QtGui.QColor(QtCore.Qt.blue)
        item.setBackground(QtGui.QColor('cyan'))
        item.setTextAlignment(QtCore.Qt.AlignHCenter)
        item.setToolTip("Blue color item")
        item.setFont(QtGui.QFont("Times", 10, QtGui.QFont.DemiBold))
        list_model.appendRow(item)


def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

