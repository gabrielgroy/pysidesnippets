'''
Simple List View - Delegate
    To edit the data of a cell using a widget we need to create a Delegate

    For Delegates we can subclass QAbstractItemDelegate or QStyledItemDelegate
    With QAbstractItemDelegate we need to reimplement paint() and sizeHint()
    But for simple widget-based delegates its better to use QStyledItemDelegate which already implements those.

    With QStyledItemDelegate we still need to reimplement some other functions.
    We need to provide an editor with the function createEditor()
    If we need different widgets for different columns or row, this is where we decide that
    The 'setEditorData()' function is the one that will set the initial data into the widget
    The 'setModelData()' function modifies the model data with the data obtained from the widget

'''
import sys 
from Qt import QtCore, QtWidgets

class SpinBoxDelegate(QtWidgets.QStyledItemDelegate):
    def createEditor(self, parent, option, index):
        editor = QtWidgets.QSpinBox(parent)
        editor.setMinimum(0)
        editor.setMaximum(100)
        return editor

    def setEditorData(self, editor, index):
        value = index.model().data(index, QtCore.Qt.EditRole)
        editor.setValue(value)

    def setModelData(self, editor, model, index):
        editor.interpretText()
        value = editor.value()
        model.setData(index, value, QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        '''
        It is the responsibility of the delegate to manage the editor's geometry.
        The geometry must be set when the editor is created, and when the item's size or position
        in the view is changed. Fortunately, the view provides all the necessary geometry information
        inside a view 'option' object.
        '''
        editor.setGeometry(option.rect)

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)

        # Create list, using the Model/View framework
        list_data = [1, 2, 3, 4]
        list_model = MyListModel(list_data, self)
        list_view = QtWidgets.QListView()
        list_view.setSpacing(5)
        
        # Connect the view with the model
        list_view.setModel(list_model)

        # We add the delegate
        delegate = SpinBoxDelegate()
        list_view.setItemDelegate(delegate)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(list_view)
        self.setLayout(layout)

class MyListModel(QtCore.QAbstractListModel):
    def __init__(self, datain, parent=None, *args):
        """ datain: a list where each item will be a row
        """
        QtCore.QAbstractListModel.__init__(self, parent, *args)
        self.listdata = datain

    # When we subclass QAbstractListModel, we need to reimplement at lease: 'rowCount' and 'data'
    
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.listdata)

    def data(self, index, role):
        if index.isValid() and role in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):
            return self.listdata[index.row()]
        else:
            return None

    # When we double click an item to edit, the flags function is called to check
    # if the item has the ItemIsEditable flag or not. If not, we won't be able to edit
    def flags(self, index):
        if not index.isValid():
            return None
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable

    # To actually edit the data, this method needs to be reimplemented too
    def setData(self, index, value, role):
        # The index needs to be valid
        if not index.isValid():
            return False
        # and the role has to be EditRole
        if role != QtCore.Qt.EditRole:
            return False
        # From the index we get the info we need to edit the underlying data
        self.listdata[index.row()] = value
        # After modifying the data we have to explicitly emit the signal dataChanged
        # The first arg is the topleft index and the second arg the bottomright index
        # In this case, they're the same
        self.dataChanged.emit(index, index)
        return True

def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

