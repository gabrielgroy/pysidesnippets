'''
Simple List Widget example with threads
    It shows how to create a simple list view using QListWidget,
    triggering the fill of the list with a button, and filling
    it from another thread using signals

This is a variant of 'listWidget_fill_w_threads.py' where we add another option to DEBUG.
    Debugging using threads is a hassle, so we want to have the option to run everything
    in the main thread sequentially, so we can add a breakpoint inside the worker's run
    function.
    Here, Worker is a QObject instead of a QThread.
    It implies more code lines to create a thread and connect it with
    the object, but allows to choose if using threads or not, for debugging
    purposes.

    I personally don't see any benefit on this method yet, but wanted to document it.
'''
import sys
import time
from Qt import QtCore, QtWidgets

ENABLE_QTHREADS = True

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        # List
        self.listWidget = QtWidgets.QListWidget(self)

        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.listWidget)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        # Worker setup
        self.worker = Worker()
        self.worker.addItem.connect(self.listWidget.addItem)
        self.worker.updateProgress.connect(self.progressBar.setValue)
        if ENABLE_QTHREADS:
            self.thread = QtCore.QThread()
            self.worker.moveToThread(self.thread)
            self.worker.finished.connect(self.thread.quit)
            self.worker.finished.connect(self.worker.deleteLater)
            self.thread.started.connect(self.worker.run)
            self.thread.finished.connect(self.thread.deleteLater)
            self.pushButton.clicked.connect(self.thread.start)
        else:
            self.pushButton.clicked.connect(self.worker.run)
            self.worker.finished.connect(self.worker.deleteLater)

class Worker(QtCore.QObject):
    addItem = QtCore.Signal(object)
    updateProgress = QtCore.Signal(int)
    finished = QtCore.Signal()
    
    def run(self):
        for i in range(10):
            item = QtWidgets.QListWidgetItem(str(i+1))
            self.addItem.emit(item)
            self.updateProgress.emit(10*(i+1))
            print("Added item {}".format(i+1))
            time.sleep(.25)
        self.finished.emit()
    

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()