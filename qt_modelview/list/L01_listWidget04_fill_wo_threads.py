'''
Simple List Widget example with threads
    It shows how to create a simple list view using QListWidget,
    triggering the fill of the list with a button
'''
import sys
import time
from Qt import QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        # List
        self.listWidget = QtWidgets.QListWidget(self)

        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")
        self.pushButton.clicked.connect(self.runFillProcess)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.listWidget)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

    def runFillProcess(self):
        for i in range(10):
            item = QtWidgets.QListWidgetItem(str(i+1))
            self.listWidget.addItem(item)
            self.progressBar.setValue(10*(i+1))
            time.sleep(.25)


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()