'''
Simple List View example
    It shows how to create a simple list view using QListView and QAbstractListModel,
    triggering the fill of the list with a button, and filling
    it from another thread using signals
'''
import sys
import time
from Qt import QtCore, QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)
        self.resize(500, 500)

        # Create list, using the Model/View framework
        self.list_model = MyListModel([], self)
        self.list_view = QtWidgets.QListView()
        
        # Connect the view with the model
        self.list_view.setModel(self.list_model)

        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.list_view)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        # Worker setup
        self.worker = Worker()
        self.worker.addItem.connect(self.list_model.addItem)
        self.worker.updateProgress.connect(self.progressBar.setValue)
        self.pushButton.clicked.connect(self.worker.start)

class MyListModel(QtCore.QAbstractListModel):
    def __init__(self, datain, parent=None, *args):
        """ datain: a list where each item will be a row
        """
        QtCore.QAbstractListModel.__init__(self, parent, *args)
        self.listdata = datain

    # Some methods need to be reimplemented when we subclass QAbstractListModel
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.listdata)

    # This method will be used by the view to query the model. It is necessary
    def data(self, index, role):
        if index.isValid() and role == QtCore.Qt.DisplayRole:
            return self.listdata[index.row()]
        else:
            return None
    
    # Connecting to this method we can add items to the model from other threads
    def addItem(self, new_data):
        self.listdata.append(new_data)
        index = self.createIndex(len(self.listdata), 0, QtCore.QModelIndex())
        self.dataChanged.emit(index, QtCore.Qt.DisplayRole)

class Worker(QtCore.QThread):

    # This is the signal that will be emitted during the processing.
    # By including int as an argument, it lets the signal know to expect
    # an integer argument when emitting.
    updateProgress = QtCore.Signal(int)
    # This signal will send new items to the model
    # (adding them to the model from here directly would not be thread safe)
    addItem = QtCore.Signal(int)

    # You can do any extra things in this init you need, but for this example
    # nothing else needs to be done expect call the super's init
    def __init__(self):
        QtCore.QThread.__init__(self)

    # A QThread is run by calling it's start() function, which calls this run()
    # function in it's own "thread". 
    def run(self):
        #Notice this is the same thing you were doing in your progress() function
        for i in range(10):
            #Emit the signal so it can be received in the main thread
            self.addItem.emit(i+1)
            self.updateProgress.emit(10*(i+1))
            time.sleep(.25)


def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

