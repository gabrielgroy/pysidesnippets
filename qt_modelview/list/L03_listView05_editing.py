'''
Simple List View example
    It shows how to create a simple list view using QListView and QAbstractListModel

    To enable editing of the items, we need to reimplement the methods 'flags' and 'setData'
    The returned flags need to include QtCore.Qt.ItemIsEditable
    'setData' is in charge of storing the edited data in the model
'''
import sys 
from Qt import QtCore, QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)

        # Create list, using the Model/View framework
        list_data = [1, 2, 3, 4]
        list_model = MyListModel(list_data, self)
        list_view = QtWidgets.QListView()
        
        # Connect the view with the model
        list_view.setModel(list_model)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(list_view)
        self.setLayout(layout)

class MyListModel(QtCore.QAbstractListModel):
    def __init__(self, datain, parent=None, *args):
        """ datain: a list where each item will be a row
        """
        QtCore.QAbstractListModel.__init__(self, parent, *args)
        self.listdata = datain

    # When we subclass QAbstractListModel, we need to reimplement at lease: 'rowCount' and 'data'
    
    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.listdata)

    # When we double click an item to edit, the flags function is called to check
    # if the item has the ItemIsEditable flag or not. If not, we won't be able to edit
    def flags(self, index):
        if not index.isValid():
            return None
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable

    def data(self, index, role):
        if index.isValid() and role == QtCore.Qt.DisplayRole:
            return self.listdata[index.row()]
        else:
            return None

    # To actually edit the data, this method needs to be reimplemented
    def setData(self, index, value, role):
        # The index needs to be valid
        if not index.isValid():
            return False
        # and the role has to be EditRole
        if role != QtCore.Qt.EditRole:
            return False
        # From the index we get the info we need to edit the underlying data
        row = index.row()
        self.listdata[index.row()] = value
        # After modifying the data we have to explicitly emit the signal dataChanged
        # The first arg is the topleft index and the second arg the bottomright index
        # In this case, they're the same
        self.dataChanged.emit(index, index)
        return True

def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

