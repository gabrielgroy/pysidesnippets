'''
Simple List Widget example with threads
    It shows how to create a simple list view using QListWidget,
    triggering the fill of the list with a button, and filling
    it from another thread using signals

This is a variant of 'listWidget_fill_w_threads.py' where we add an option to DEBUG
    Debugging using threads is a hassle, so we want to have the option to run everything
    in the main thread sequentially, so we can add a breakpoint inside the worker's run
    function.
    One simple approach is to connect the button that triggers the thread to the "run"
    function instead of the "start" function. This way, even if the object is a QThread, it
    is not run in it's own thread.

    Note that setting ENABLE_QTHREADS to False, the UI is not refreshed when the items get added
'''
import sys
import time
from Qt import QtCore, QtWidgets

ENABLE_QTHREADS = True

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        # List
        self.listWidget = QtWidgets.QListWidget(self)

        # Progress bar
        self.progressBar = QtWidgets.QProgressBar(self)
        self.progressBar.setValue(0)
        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

        # Button to trigger process start
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Push to fill")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.listWidget)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.pushButton)
        self.setLayout(layout)

        # Worker setup
        self.worker = Worker()
        self.worker.addItem.connect(self.listWidget.addItem)
        self.worker.updateProgress.connect(self.progressBar.setValue)
        # This is where the modification is added
        if ENABLE_QTHREADS:
            self.pushButton.clicked.connect(self.worker.start)
        else:
            self.pushButton.clicked.connect(self.worker.run)

class Worker(QtCore.QThread):
    addItem = QtCore.Signal(object)
    updateProgress = QtCore.Signal(int)
    
    def run(self):
        for i in range(10):
            item = QtWidgets.QListWidgetItem(str(i+1))
            self.addItem.emit(item)
            self.updateProgress.emit(10*(i+1))
            print("Added item {}".format(i+1))
            time.sleep(.25)
    

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()