'''
Simple List View example
    It shows how to create a simple list view using QListView and QStandardItemModel

    QStandardItemModel requires to create the contained items with QStandardItem and
    add them to the model.
    It's similar to using QListWidget.

    This view does not display horizontal or vertical headers
'''
import sys 
from Qt import QtCore, QtGui, QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)

        # Create list, using the Model/View framework
        list_data = [1, 2, 3, 4]
        list_model = QtGui.QStandardItemModel()
        list_view = QtWidgets.QListView()
        
        # Connect the view with the model
        list_view.setModel(list_model)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(list_view)
        self.setLayout(layout)

        # Fill data
        list_model.setRowCount(len(list_data)) # Optional
        for row, item in enumerate(list_data):
            item = QtGui.QStandardItem(str(list_data[row]))
            list_model.setItem(row, item)


def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

