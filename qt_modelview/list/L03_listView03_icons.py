'''
Simple List View example
    With QListView and QAbstractListModel we can still add a widget, but
    the problem is we need to add it from the view, not the model.
    This is only useful for static data

    In this example, the first row has an icon that is provided by the model
    in the decoration role

    The second row icon is a QLabel widget. The model doesn't know about it.
    But we still need to provide a size hint from the model, otherwise there is
    no room for the icon.
'''
import os
import sys 
from Qt import QtCore, QtGui, QtWidgets

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")
icon_car = os.path.join(basedir, "../../resources/car.png")
icon_house = os.path.join(basedir, "../../resources/house.png")

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)

        # Create list, using the Model/View framework
        list_model = MyListModel(self)
        list_view = QtWidgets.QListView()
        # Icon size is defined in the list view widget
        list_view.setIconSize(QtCore.QSize(100,100))

        # The list can be changed into Icon Mode:
        # list_view.setViewMode(QtWidgets.QListView.IconMode)
        # list_view.setResizeMode(QtWidgets.QListView.Adjust)
        
        # Connect the view with the model
        list_view.setModel(list_model)

        # We get the index from the item
        index = list_model.index(1, 0, QtCore.QModelIndex())
        # We now create the label with the image
        icon_pixmap = QtGui.QPixmap(icon_boat)#.scaledToHeight(ICON_SIZE)
        label = QtWidgets.QLabel(self)
        label.setPixmap(icon_pixmap)
        label.setAlignment(QtCore.Qt.AlignCenter)
        label.setContentsMargins(0,0,0,0)
        label.setAutoFillBackground(False)
        # And finally we add the widget to the item/index
        list_view.setIndexWidget(index, label)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(list_view)
        self.setLayout(layout)

class MyListModel(QtCore.QAbstractListModel):
    def __init__(self, parent=None, *args):
        """ datain: a list where each item will be a row
        """
        QtCore.QAbstractListModel.__init__(self, parent, *args)

    def rowCount(self, parent=QtCore.QModelIndex()):
        return 2

    def data(self, index, role):
        if not index.isValid():
            return
        if index.row() == 0:
            if role == QtCore.Qt.DecorationRole:
                return QtGui.QIcon(icon_boat)
        if index.row() == 1:
            if role == QtCore.Qt.SizeHintRole:
                return QtCore.QSize(100,70)
        

def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

