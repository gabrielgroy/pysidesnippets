'''
Simple List View example
    To edit the items of a list view/model, we simply need to set the appropriate flags
    The thing with the flags though, is that there are some set by default, so to add
    one more, we also need to explicitly set the default ones, ore they get lost

    The default ones are:
    - QtCore.Qt.ItemIsEnabled
    - QtCore.Qt.ItemIsSelectable
    - QtCore.Qt.ItemIsDragEnabled
    - QtCore.Qt.ItemIsDropEnabled
    - QtCore.Qt.ItemIsUserCheckable

    The modified data is stored in the items. So to obtain the data, we need to retrieve
    it from the items of the model, not the model itself
'''
import sys 
from Qt import QtCore, QtGui, QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        QtWidgets.QWidget.__init__(self, *args)

        # Create list, using the Model/View framework
        list_data = [1, 2, 3, 4]
        self.list_model = QtGui.QStandardItemModel()
        list_view = QtWidgets.QListView()
        
        # Connect the view with the model
        list_view.setModel(self.list_model)

        # Button
        button = QtWidgets.QPushButton(self)
        button.setText("Print Data")

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(list_view)
        layout.addWidget(button)
        self.setLayout(layout)

        button.clicked.connect(self.printData)

        # Fill data
        self.list_model.setRowCount(len(list_data)) # Optional
        for row, item in enumerate(list_data):
            item = QtGui.QStandardItem(str(list_data[row]))
            item.setFlags(\
                QtCore.Qt.ItemIsEnabled\
                | QtCore.Qt.ItemIsEditable\
                # | QtCore.Qt.ItemIsSelectable\
                # | QtCore.Qt.ItemIsDragEnabled\
                # | QtCore.Qt.ItemIsDropEnabled\
                # | QtCore.Qt.ItemIsUserCheckable\
                )
            self.list_model.setItem(row, item)

    def printData(self):
        for i in range(self.list_model.rowCount()):
            item = self.list_model.item(i)
            print(item.text())


def main():
    app = QtWidgets.QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

