'''
Simple List Widget example
    It shows how to create a simple list view using QListWidget
'''
import sys
from Qt import QtWidgets

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        listWidget = QtWidgets.QListWidget(self)

        # Opt 1, passing in the listWidget as the parent when creating the items
        for item_data in [1,2,3,4]:
            item = QtWidgets.QListWidgetItem(str(item_data), listWidget)
        
        # Opt 2, creating the items without parent, and adding them to the widget later
        for item_data in [5,6,7,8]:
            item = QtWidgets.QListWidgetItem(str(item_data))
            listWidget.addItem(item)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(listWidget)
        self.setLayout(layout)

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()
