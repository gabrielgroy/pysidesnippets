'''
Simple List Widget example
    Icons

    We can add icons to the items in a list with the function:
    item.setIcon
    But if there is no text and we simply want to see the icon, it
    will still be at the left side of the cell.

    If we want to have further control over the icon, we can use a 
    label widget and add it to the cell. To do so, we need to get access
    to the index from the item, and use:
    listWidget.setIndexWidget(index, icon_label_widget)
'''
import os
import sys
from Qt import QtCore, QtGui, QtWidgets

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../../resources/boat.png")
icon_car = os.path.join(basedir, "../../resources/car.png")
icon_house = os.path.join(basedir, "../../resources/house.png")

ICON_SIZE = 50

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        listWidget = QtWidgets.QListWidget(self)
        # Icon size is defined in the list widget
        listWidget.setIconSize(QtCore.QSize(ICON_SIZE,ICON_SIZE))
        listWidget.setAlternatingRowColors(True)

        ## Icon in the cell in the standard way
        item = QtWidgets.QListWidgetItem()
        item.setIcon(QtGui.QIcon(icon_boat))
        listWidget.addItem(item)

        ## Icon in the cell through a widget
        item = QtWidgets.QListWidgetItem()
        # As there is no text there is no size hint,
        # so we need to provide one 
        item.setSizeHint(QtCore.QSize(ICON_SIZE,ICON_SIZE))
        listWidget.addItem(item)
        # We get the index from the item
        index = listWidget.indexFromItem(item)
        # index = self.model().index(row, 0, QtCore.QModelIndex())
        # We now create the label with the image
        icon_pixmap = QtGui.QPixmap(icon_boat)#.scaledToHeight(ICON_SIZE)
        label = QtWidgets.QLabel(self)
        label.setPixmap(icon_pixmap)
        label.setAlignment(QtCore.Qt.AlignCenter)
        label.setContentsMargins(0,0,0,0)
        label.setAutoFillBackground(False)
        # And finally we add the widget to the item/index
        listWidget.setIndexWidget(index, label)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(listWidget)
        self.setLayout(layout)


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()
