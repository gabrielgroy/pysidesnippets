'''
In this example, instead of creating the UI with code, we create it
with Qt's Designer, generating a .ui file
Then, we translate the ui file into code on the fly with the
function loadUiType
'''
import os.path
import sys 
from PySide import QtCore
from PySide import QtGui
from loadUiType import loadUiType

# We build the path of the UI file from the path of this file
UIFILE = os.path.join(os.path.dirname(__file__), 'minimum04.ui').replace('\\', '/')

def main(): 
    # loadUiType will return two objects:
    #   - the first one is the form or dialog we designed in Designer
    #     In other words, the ui file translated into python code
    #   - the second one is the dialog or widget class (QDialog, QWidget, etc)
    window_form, window_base = loadUiType(UIFILE)

    app = QtGui.QApplication(sys.argv) 

    class MyWindow(window_base):
        def __init__(self, parent=None):
            super(MyWindow, self).__init__(parent)
            self.ui = window_form() # the ui items are loaded in the self.ui variable
            # self.ui is now an instance of the window_form class, which is the dialog we
            # created in Designer. To actually create it, we need to execute it's 'setupUi' method
            self.ui.setupUi(self)
            # Now we can access it's widgets from self.ui: self.ui.<widgetname>

    w = MyWindow()
    w.show() 
    sys.exit(app.exec_()) 


if __name__ == "__main__": 
    main()

