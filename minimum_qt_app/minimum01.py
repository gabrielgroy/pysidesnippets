'''
Just the minimum code to display a widget
'''
import sys
from PySide import QtGui

# A QApplication is always needed to start creating widgets
app = QtGui.QApplication([])

# We simply create and show the widget
w = QtGui.QWidget()
w.show()

# Nothing will happen until we execute the app
sys.exit(app.exec_())


