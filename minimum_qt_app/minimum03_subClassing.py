'''
Same as before, but instead of creating an empty widget
we create a subclass of QWidget in which we add some sub-widgets
In this case, two buttons
'''
import sys 
from PySide import QtCore
from PySide import QtGui

#################################################################### 
# This part of the code is always simple:
#   - create the app,
#   - create an instance of our dialog or window
#   - show, and execute the app
def main(): 
    app = QtGui.QApplication(sys.argv) 
    # MyWindow is a subclass of QWidget
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

#################################################################### 
# It is in our dialog or window where we really create the UI
class MyWindow(QtGui.QWidget):
    def __init__(self, *args): 
        QtGui.QWidget.__init__(self, *args) 

        # Inside the subclass, we incorporate all our widgets
        pb1 = QtGui.QPushButton()
        pb1.setText("Uno")
        pb2 = QtGui.QPushButton()
        pb2.setText("Dos")

        # we always create a layout
        layout = QtGui.QVBoxLayout()
        # then add the widgets
        layout.addWidget(pb1)
        layout.addWidget(pb2)
        # and assign the layout to the main window (self in this case)
        self.setLayout(layout)

####################################################################
if __name__ == "__main__": 
    main()

