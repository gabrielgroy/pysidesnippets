'''
This is the same, but introducing the Qt application inside a function
'''
import sys
from PySide import QtGui

def main(): 
    app = QtGui.QApplication(sys.argv) 
    w = QtGui.QWidget() 
    w.show() 
    sys.exit(app.exec_()) 


if __name__ == "__main__": 
    main()

