# pysideSnippets

Some QT / Pyside examples and snippets

For the Qt imports I'm using a wrapper, so that the code is valid with the different Python Qt bindings.

Feel free to modify `from Qt import ...` by `from PySide import ...` or whatever version you're using.
