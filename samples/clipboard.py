'''
To copy something into the clipboard, we can use the QApplication clipboard
'''
import sys
from Qt import QtWidgets

class MyWindow(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super(MyWindow, self).__init__(parent=parent)

    def onCopyPath(self):
        '''
        Copies into memory the path of the folder
        containing the files displayed
        '''
        text_to_copy = "Some Text"
        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.setText(text_to_copy)

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__ == "__main__": 
    main()

