'''
If we want to do some actions when a window is closed, like for instance save some settings,
we need to reimplement the CLOSE event
'''

import sys 
from Qt import QtWidgets


class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

    def closeEvent(self, event):
        print("Closing window")

def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 

if __name__ == "__main__": 
    main()

