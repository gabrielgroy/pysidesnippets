import os
import sys
from Qt import QtCore, QtGui, QtWidgets

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../resources/boat.png")

ICON_SIZE = 50

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        layout = QtWidgets.QVBoxLayout()
        texts = [
            '<html><head/><body>  <p>The Playlist was created successfully. Access it through any of this links:</p>  <p><a href="http://wwww.google.com"><span style=" text-decoration: underline; color:rgb(20,150,255);">Link to the Playlist in the Media page</span></a></p>        <p><a href="http://wwww.google.com"><span style=" text-decoration: underline; color:#0000ff;">Link to the Playlist detail information</span></a></p></body></html>',
            ''
        ]
        for text in texts:
            label = QtWidgets.QLabel(self)
            label.setText(text)
            layout.addWidget(label)

        self.setLayout(layout)


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow()
    w.resize(400,300)
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()
