import os
import sys
from Qt import QtCore, QtGui, QtWidgets

basedir = os.path.dirname(__file__)
icon_boat = os.path.join(basedir, "../resources/boat.png")

ICON_SIZE = 50

class MyWindow(QtWidgets.QWidget):
    def __init__(self, *args): 
        QtWidgets.QWidget.__init__(self, *args) 

        self.label = QtWidgets.QLabel(self)
        self.label.setText("Text ...........................")
        icon_pixmap = QtGui.QPixmap(icon_boat)
        self.label.setPixmap(icon_pixmap)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setContentsMargins(0,0,0,0)
        # self.label.setAutoFillBackground(False)

        # layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.label)
        self.setLayout(layout)


def main(): 
    app = QtWidgets.QApplication(sys.argv) 
    w = MyWindow()
    w.resize(400,300)
    w.show() 
    sys.exit(app.exec_()) 

if __name__=='__main__':
    main()
