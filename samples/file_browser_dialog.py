import sys
from Qt import QtWidgets

def main(): 
    app = QtWidgets.QApplication(sys.argv)

    ## Option 1, using the static function getOpenFileNames() or getOpenFileName()
    ## ----------------------------------------------------------------------------
    # Args:
    # - parent
    # - title
    # - starting directory, if omitted (the dir of the py file is used)
    # - filters (we can add many separating them with ;;)
    filenames, filters = QtWidgets.QFileDialog().getOpenFileNames(None, "Open Images",
        'C:/tmp', "Images (*.png *.xpm *.jpg);;All Files (*)")

    ## Option 2, instancing the dialog
    ## ------------------------------------
    w = QtWidgets.QFileDialog()
    w.setFileMode(QtWidgets.QFileDialog.ExistingFiles) # AnyFile, Directory, ExistingFile, ExistingFiles
    w.setDirectory('C:/tmp')
    w.setNameFilter("Images (*.png *.xpm *.jpg)")
    # w.setOption(QtWidgets.QFileDialog.DontUseNativeDialog)
    filenames = []
    if w.exec_():
        filenames = w.selectedFiles()

    print("Selected files:")
    for f in filenames:
        print(f)

    sys.exit(app.exec_()) 


if __name__ == "__main__": 
    main()

