import os, sys
from PySide import QtGui, QtCore


class DropLabelWidget(QtGui.QLabel):
    ''' class to handle the drop of files. It will trigger a process for each dropped file
    '''

    # signal to emit on drop
    newFile = QtCore.Signal(str)

    def __init__(self, extensions=None, parent=None):  # @UnusedVariable
        ''' init widget '''
        super(DropLabelWidget, self).__init__(parent)
        self.setAcceptDrops(True)
        self.acceptedExtensions = extensions or []
        self.setStyleSheet("background-color: rgb(100, 50, 50);")
        self.setFrameStyle(QtGui.QFrame.Panel)
        self.setText("Drag a file here")
        self.setAlignment(QtCore.Qt.AlignCenter)
        
    def setExtensions(self, exts):
        self.acceptedExtensions = exts

    def getExtensions(self, exts):
        return self.acceptedExtensions

    def dragEnterEvent(self, event):
        ''' event needed for drag and drop behaviour '''
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        ''' event needed for drag and drop behaviour '''
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        ''' event needed for drag and drop behaviour '''
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()

            # expand all the paths and call the function to emit the signal
            links = expandUrlsData(event.mimeData().urls())
            self.fileDropped(links)
        else:
            event.ignore()

    def fileDropped(self, path_list):
        ''' Filter out the paths that are invalid and emit the signal with the list so
        the main ui can process the files
        Args:
            path_list (list[unicode]): the list of full paths to process
        Returns:
            None
        '''
        if self.acceptedExtensions:
            filtered_path_list = [path for path in path_list if filterPaths(path, self.acceptedExtensions)]
        else:
            filtered_path_list = path_list[:]
        if filtered_path_list:
            self.gotNewFile(filtered_path_list[0])
            self.setStyleSheet("background-color: rgb(50, 100, 50);")
        else:
            self.gotNewFile("")
            self.setStyleSheet("background-color: rgb(100, 50, 50);")
            
    def gotNewFile(self, file):
        if file:
            self.setText(os.path.basename(file))
        else:
            self.setText("Drag EDL here")
            
        self.newFile.emit(file)
        

def expandUrlsData(url_list):
    ''' expands the list of files or the files inside a folder
    Args:
        url_list (list[unicode]): a list with all the urls dropped onto the widget
    Returns:
        list[unicode]: a list of full path files
    '''
    result = []
    for url in url_list:
        path = unicode(url.toLocalFile())
        if os.path.isdir(path):
            result.extend(listDir(path))
        else:
            result.append(path)
    return result


def listDir(path):
    ''' List all the files in the folder
    Args:
        path (unicode): full path to the folder
    Returns:
        list[unicode]: list of full path files
    '''
    file_list = os.listdir(path)
    full_path_list = [os.path.join(path, file_) for file_ in file_list]
    full_path_file_list = [
        file_ for file_ in full_path_list if os.path.isfile(file_)]
    return full_path_file_list


def filterPaths(path, extensions):
    ''' place holder to filter out files we dont want to process
    Args:
        path (unicode): full path to the file to analyze
        extensions (list): 
    Returns:
        bool: whether or not this file should be processed
    '''
    ext = os.path.splitext(os.path.basename(path))[-1]
    return ext in extensions


class MyWindow(QtGui.QWidget):
    def __init__(self, *args): 
        QtGui.QWidget.__init__(self, *args) 
        self.label = DropLabelWidget(parent=self)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.label)
        self.setLayout(layout)

if __name__=='__main__':
    app = QtGui.QApplication(sys.argv) 
    w = MyWindow() 
    w.resize(400,300)
    w.show() 
    sys.exit(app.exec_()) 
